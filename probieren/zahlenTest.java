package probieren;

import java.io.IOException;
import java.util.Scanner;

public class zahlenTest {

	public static void main(String[] args) {
		// Alle Zahlen ausgeben die gerade sind ab der Zahl die man eingibt. und die
		// summe berechnen.
		// fuer falsche zahlen und ungueltige eingaben exception auswerfen
		// zahlen nur zwischen 1000 und 2000 erlaubt

		String eingabe = "";
		String ausgabe = "";
		int summe = 0;
		int zahl = 0;
		boolean fehler = false;
		Scanner leser = new Scanner(System.in);

		do {
			fehler = false;
			try {
				System.out.println("Gebe eine Zahl zwischen 1000 und 2000 ein: ");
				eingabe = leser.nextLine();
				zahl = Integer.parseInt(eingabe);
				if (zahl < 1000 || zahl > 2000) {
					throw new IllegalArgumentException("Diese Zahl ist nicht erlaubt!");
				}
			}

			catch (Exception e) {
				System.out.printf("%s%n", e.getMessage());
				fehler = true;
			}

		}

		while (fehler);

		while (zahl >= 1000 && zahl <= 2000) {
			if (zahl % 2 == 0) {
				ausgabe = ausgabe + " " + zahl;
				summe = summe + zahl;
			}
			zahl++;
		}
		System.out.println(ausgabe);
		System.out.println(summe);
		leser.close();

	}

}
