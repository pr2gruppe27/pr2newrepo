package komplex;

import java.io.IOException;

public class KomplexeZahlKlein2 extends komplexeZahl {

	KomplexeZahlKlein2(double real, double imagi) {
		super(real, imagi);

		if (imagi > 10) {
			try {
				KZKException();
			} catch (KZKException e) {
				System.out.println("Fehler: " + e.getMessage());

			}
		}
	}

	private void KZKException() throws KZKException {
		throw new KZKException();
	}

	private String getMessage() {
		return String.format("Ihr real Anteil: " + " Dieser Imagineare Anteil(" + ") ist zu gross fuer diese Klasse");
	}

}
