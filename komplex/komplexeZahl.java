package komplex;

public class komplexeZahl {

	protected double real;
	protected double imagi;

// constructor komplexZahl
// *******************************************************
// brainstorming:
// this type of number is a part real and a part imagine(unreal)
// the real numbers are at the X-Axis
// imagine numbers are at Y-Axis
// *******************************************************

	komplexeZahl(double real, double imagi) {
		this.real = real;
		this.imagi = imagi;

	}

	protected double re() {
		return this.real;
	}

	protected double im() {
		return this.imagi;
	}
//	protected komplexeZahl getName() {
//		return this;
//	}

//*************************************
//number print as toString method. 
//!it gives not the calculated number!

	public String toString() {
		return String.format(
				" " + "=" + real + "+(" + imagi + "*i)" + "     Die Darstellung ist nach algebra Vorbild x=A+Bi");
	}
//****************************************
//Komplexe Number addition komplexe Number

	public komplexeZahl add(komplexeZahl zahl) {
		return new komplexeZahl(this.real + zahl.real, this.imagi + zahl.imagi);

	}

//****************************************
//Komplexe Number multiplication komplexe Number

	public komplexeZahl prod(komplexeZahl zahl) {
		return new komplexeZahl(this.real * zahl.real - this.imagi * zahl.imagi,
				this.real * zahl.imagi + this.imagi * zahl.real);
	}

//****************************************
//Komplexe Number addition komplexe Number als static

	public static komplexeZahl addStatic(komplexeZahl zahl, komplexeZahl zahl2) {
		return new komplexeZahl(zahl.real + zahl2.real, zahl.imagi + zahl2.imagi);
	}

//****************************************
//Komplexe Number multiplication komplexe Number als static

	public komplexeZahl prodStatic(komplexeZahl zahl, komplexeZahl zahl2) {
		return new komplexeZahl(zahl.real * zahl2.real - zahl.imagi * zahl2.imagi,
				zahl.real * zahl2.imagi + zahl.imagi * zahl2.real);
	}

}
