package pr2.pu2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Zoo implements Iterable<Tier> {

// Instanzvariable
	protected Map<String, ArtenGehege> tierMap = new HashMap<>();

	// iterator tier soll uns alle tiere liefern und sie dann nach dem
	// leben der einzelnen tiere sortieren
	// um zugriff auf die tiere in den gehegen zu bekommen
	// machen wir einen iterator<Tier> der tiere erwaretet
	// wie bekommen wir die tiere?
	@Override
	public Iterator<Tier> iterator() {
		List<Tier> hasenListe = new ArrayList<>(); // neue tierListe und diese tierListe
		for (ArtenGehege gehege : tierMap.values()) {// for each schleife liefert gehege
			hasenListe.addAll(gehege.hasenList); // tierListe wird mit methode
													// addAll-> also alles was drinnen ist
													// in dem fall zugriff auf die tierList
													// die instanz aus Klasse ArtenGehege
		}
		// ein comparator der die tiere sortiert

		Comparator<Tier> compTier = new Comparator<Tier>() {

			@Override
			public int compare(Tier o1, Tier o2) {
				if (o1.getLeben() == o2.getLeben()) {
					return 0;
				} else if (o1.getLeben() > o2.getLeben()) {
					return -1;
				} else
					return +1;

			}

		};
		// die java collections methode sort sortiert die ubergebene liste mit hilfe der
		// int werte des comperator compTier
		Collections.sort(hasenListe, compTier);

		// dieser iterator geht durch die tierListe durch
		return new Iterator<Tier>() {

			int pos = 0;

			@Override
			public boolean hasNext() {

				return (pos < hasenListe.size());
			}

			@Override
			public Tier next() {

				pos = pos + 1;
				return hasenListe.get(pos - 1);
			}

		};
	}

}
