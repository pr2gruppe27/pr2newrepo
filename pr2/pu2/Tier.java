package pr2.pu2;

import java.text.DecimalFormat;
import java.util.Random;

public class Tier implements Runnable {

	// Alle Instanz und Klassenvariablen sollen die sichbarkeit protected also
	// sichbarkeit innerhalb
	// der klasse haben
	
	protected static int anzahlDerTiere = 0;
	protected int leben;
	///////////////////////
	protected int x;
	protected int y;

	protected Thread self;
	// ???????????????????????????????????????????????????????Aufgabe 2
	// Weiterhin braucht die Tier-Klasse noch eine Instanzvariable, in der eine
	// Referenz auf
	// den eigenen Thread gespeichert

	// Konstruktor Tier bekommt ein leben int wert
	protected Tier(int x, int y, int leben) {
		// *************************
		this.x = x;
		this.y = y;
		this.leben = leben;
	}
	
	public int getX() {
		return this.x;
	}
	public int getY( ) {
		return this.y;
	}
	public int getLeben() {
		return this.leben;
	}
	public String toString() {
		return String.format(""+ this.self + "X: "+
	this.x +" Y: "+this.y + " Leben: "+this.leben);
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		while (this.leben >= Const.TIERDEAD) {
//			System.out.println(leben);

			this.leben--;
			try {
				Thread.sleep(100);
			

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	// die methode move sorgt dafuer das tier sich bewegen in ihrem lebenszyklus
	// wird bei jedem neuen Tier mit aufgerufen
	public void move(Tier tier) {
		int random = new Random().nextInt(5);
		if (random == 1 && this.x > 0) {
			this.x = -1;
		} else if (random == 2) {
			this.x = +1;
		} else if (random == 3 && this.y > 0) {
			this.y = -1;
		} else if (random == 0) {
			this.y = +1;
		} else {
			this.x = 0;
			this.y = 0;
		}
	}

	// **********************************************************
	// Die factory methode zum erstellen von tieren die als neben programm laufen
	public static Tier create() throws InterruptedException {
		anzahlDerTiere++;
		int random = new Random().nextInt(101);
		DecimalFormat dF = new DecimalFormat("000");
		String tierNumber = dF.format(0);
		Tier neuesTier = new Tier(0, 0, random);
		neuesTier.move(neuesTier);
		Thread threads = new Thread(neuesTier);
		tierNumber = dF.format(anzahlDerTiere);
		threads.setName("Tier-" + "" + tierNumber);
//		 System.out.println(neuesTier.self);
		neuesTier.self = threads;
		threads.start();
		return neuesTier;
	}

//************************************
//MAIN METHODE
	// ***************ACHTUNG: Das Hasen Objekt verhalt sich noch fehlerhaft!
	// Erwartet eig immer ein leben von 40 aber das funktioniert noch nicht!
	public static void main(String[] args) throws InterruptedException {
//	Tier.create();
//	Tier.create();
//	Tier.create();
//	Tier.create();
	

		// *******************************
		// Aufgabe 7
//		int test1 = 34;
//		Tier test2 = new Tier(2, 2, 3);
//		Tier test3 = new Tier(2, 2, 23);
//		Tier test4 = new Tier(2, 2, 78);
		
//		Hase hase1 = new Hase(40);
//		Hase hase2 = new Hase(40);
//		Hase hase3 = new Hase(40);
		var gehege = new ArtenGehege<Tier>();
		
//		gehege.einsperren(Tier.create());
//		gehege.einsperren(test3);
//		gehege.einsperren(test4);
		gehege.einsperren(Tier.create());
		gehege.einsperren(Hase.create());
		gehege.einsperren(Tier.create());
	   
		var zoo = new Zoo();
		zoo.tierMap.put("Gehege", gehege);
		var it = zoo.iterator();
		while(it.hasNext()) {
			System.out.println(it.next().toString());
		}
	    
		
		// hier sollte eigentlich meckern
//	gehege.einsperren(test1);
//	gehege.einsperren(Tier.create());
		// ***********************************
		
//         gehege.print();

//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();
//	TierFactory.create();

//	System.out.println(anzahlDerTiere);
	}

}
