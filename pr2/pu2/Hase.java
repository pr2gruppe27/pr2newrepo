package pr2.pu2;

import java.text.DecimalFormat;
import java.util.Random;

public class Hase extends Tier {
//*****************************************
//Hase soll immer leben 40 haben .---> super(40)
	
	protected Hase() {
		super(0,0,41);
		
		
	}
	
	// **********************************************************
		// Die factory methode zum erstellen von hasen die als neben programm laufen
		public static Hase create() throws InterruptedException {
			anzahlDerTiere++;
			DecimalFormat dF = new DecimalFormat("000");
			String tierNumber = dF.format(0);
			Hase neuerHase = new Hase();
			neuerHase.move(neuerHase);
			Thread threads = new Thread(neuerHase);
			tierNumber = dF.format(anzahlDerTiere);
			threads.setName("Tier-" + "" + tierNumber);
//			 System.out.println(neuesTier.self);
			neuerHase.self = threads;
			threads.start();
			return neuerHase;
		}
	
	
	

}
