package pr2.pu2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Copy {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Scanner leser = new Scanner(System.in);
//		String ursprungsdatei = "C:/users/YM-Performance/ursprung.txt";
//		String zieldatei="C:/users/YM-Performance/ziel.txt";
		String nameHomeVerzeichnis = System.getProperty("user.home");
//		System.out.println(nameHomeVerzeichnis);
		String ursprungsdatei;
		String zeile;
		String zieldatei;

		System.out.println("Gebe Ursprungsdatei: ");
		zeile = leser.nextLine();

		Path pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
		if (Files.exists(pfadVonDatei)) {

			ursprungsdatei = pfadVonDatei.toString();
			System.out.println(pfadVonDatei);
		} else {
			leser.close();
			throw new FileNotFoundException("Datei existiert nicht");
		}

		System.out.println("Gebe Zieldatei: ");
		zeile = leser.nextLine();

		pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
		if (Files.exists(pfadVonDatei)) {

			zieldatei = pfadVonDatei.toString();
			System.out.println(pfadVonDatei);
		} else {
			leser.close();
			throw new FileNotFoundException("Datei existiert nicht");
		}

		Copy.copy(ursprungsdatei, zieldatei);

//		catch(FileAlreadyExistsException allEx) {
//			System.err.println(allEx.getMessage());
//		}
		System.out.println("Ihre Ursprungsdatei " + ursprungsdatei + " wird kopiert in: " + zieldatei);
		leser.close();
	}

	public static void copy(String ursprung, String ziel) {

		try (var in = new BufferedInputStream(new FileInputStream(ursprung));
				var out = new BufferedOutputStream(new FileOutputStream(ziel))) {
			final var buffer = new byte[in.available()];
			var gelesen = 0;
			while ((gelesen = in.read(buffer)) > -1) {
				out.write(buffer, 0, gelesen);
			}
		} catch (final FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (final IOException ex) {
			ex.printStackTrace();
		}

	}

}
