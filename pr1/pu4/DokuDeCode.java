package pr1.pu4;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class DokuDeCode {
	public static void safeToLocation(String data, Path location) {
		try (BufferedWriter writer = Files.newBufferedWriter(location)) {
			writer.write(data);
		} catch (final FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (final IOException ex) {
			ex.printStackTrace();
		}
	}

	public static String readFromLocation(Path data) {
		String ziel = "";
		try (BufferedReader reader = Files.newBufferedReader(data)) {
			ziel = reader.readLine();

		} catch (final FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (final IOException ex) {
			ex.printStackTrace();
		}
		return ziel;
	}

	public static String code(String eingabe) {
		String eingabeNeu = "";
		char[] portion = new char[eingabe.length()];
		for (int i = 0; i < eingabe.length(); i++) {

			portion[i] = eingabe.charAt(i);
		}
		int counter = 0;
		char help;
		while (counter < portion.length) {
			help = portion[counter];
			if (help == 'A' || help == 'a') {
				help = (char) (help + 25);
				portion[counter] = help;
			} else if (istBuchstabe(help) == true) {
				help = (char) (help - 1);
				portion[counter] = help;
			}
			counter++;
		}

		for (int i = 0; i < portion.length; i++) {
			eingabeNeu += "" + portion[i];
		}

		return eingabeNeu;
	}

	public static String deCode(String eingabe) {
		String eingabeNeu = "";
		char[] portion = new char[eingabe.length()];
		for (int i = 0; i < eingabe.length(); i++) {

			portion[i] = eingabe.charAt(i);
		}
		int counter = 0;
		char help;
		while (counter < portion.length) {
			help = portion[counter];
			if (help == 'Z' || help == 'z') {
				help = (char) (help - 25);
				portion[counter] = help;
			} else if (istBuchstabe(help) == true) {
				help = (char) (help + 1);
				portion[counter] = help;
			}
			counter++;
		}

		for (int i = 0; i < portion.length; i++) {
			eingabeNeu += "" + portion[i];
		}

		return eingabeNeu;
	}

	public static boolean istBuchstabe(char buchstabe) {
		boolean ja = false;
		if (buchstabe == 'A' || buchstabe == 'a' || buchstabe == 'B' || buchstabe == 'b' || buchstabe == 'C'
				|| buchstabe == 'c' || buchstabe == 'D' || buchstabe == 'd' || buchstabe == 'E' || buchstabe == 'e'
				|| buchstabe == 'F' || buchstabe == 'f' || buchstabe == 'G' || buchstabe == 'g' || buchstabe == 'H'
				|| buchstabe == 'h' || buchstabe == 'I' || buchstabe == 'i' || buchstabe == 'J' || buchstabe == 'j'
				|| buchstabe == 'K' || buchstabe == 'k' || buchstabe == 'L' || buchstabe == 'l' || buchstabe == 'M'
				|| buchstabe == 'm' || buchstabe == 'N' || buchstabe == 'n' || buchstabe == 'O' || buchstabe == 'o'
				|| buchstabe == 'P' || buchstabe == 'p' || buchstabe == 'Q' || buchstabe == 'q' || buchstabe == 'R'
				|| buchstabe == 'r' || buchstabe == 'S' || buchstabe == 's' || buchstabe == 'T' || buchstabe == 't'
				|| buchstabe == 'U' || buchstabe == 'u' || buchstabe == 'V' || buchstabe == 'v' || buchstabe == 'W'
				|| buchstabe == 'w' || buchstabe == 'X' || buchstabe == 'x' || buchstabe == 'Y' || buchstabe == 'y'
				|| buchstabe == 'Z' || buchstabe == 'z')
			ja = true;
		return ja;

	}

	// boolean kodierungsrichtung == false wenn noch nicht verschlusselt
	public static String kodieren(String text, boolean kodierungsRichtung) {
		String neuText;
		if (kodierungsRichtung == false) {
			neuText = deCode(text);
		} else {
			neuText = code(text);
		}
		return neuText;
	}

	public static void main(String[] args) throws FileNotFoundException {
//		String ursprungsdatei = "C:/users/YM-Performance/ursprung.txt";
//		String zieldatei="C:/users/YM-Performance/ziel.txt";
		String nameHomeVerzeichnis = System.getProperty("user.home");
//		System.out.println(nameHomeVerzeichnis);
		String ursprungsdatei;
		String zeile;
		String zieldatei;
		var deCodeMain = "";
		Scanner leser = new Scanner(System.in);
		String antwort;
		String beforeDeCode = "";
		String beforeCode = "";
		System.out.println("Wollen Sie einen Text eingeben?");
		System.out.println("J/N");
		antwort = leser.nextLine();
		if (antwort.equals("J")) {
			System.out.println("Sagen Sie: deCode oder Code");
			antwort = leser.nextLine();
			// es wird die methode kodieren aufgerufen die die
			// antwort verschlusselt
			if (antwort.equals("deCode")) {
				System.out.println("Gebe Text ein: ");
				antwort = leser.nextLine();
				deCodeMain = kodieren(antwort, false);
			}
			// es wird die methode kodieren aufgeruden die die antwort
			// entschlusselt
			if (antwort.equals("Code")) {
				System.out.println("Gebe Text ein: ");
				antwort = leser.nextLine();
				deCodeMain = kodieren(antwort, true);
			}

			// dieser Block wird immer ausgefuhrt egal ob Code oder deCode
			// der Benutzer wird gefragt ob er speichern will
			// wenn antwort ja dann methoden aufruf zum antwort speichern
			// in textdatei
			System.out.println("Text speichern: J/N ");
			antwort = leser.nextLine();
			if (antwort.equals("J")) {
				System.out.println("Ziel angeben: ");
				zeile = leser.nextLine();

				Path pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
				if (Files.exists(pfadVonDatei)) {
					System.out.println("Die Datei existiert, neu anlegen: J/N");
					antwort = leser.nextLine();
					if (antwort.equals("J")) {
						try {
							System.out.println("Ziel eingeben ");
							zeile = leser.nextLine();
							pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
							Files.createFile(pfadVonDatei);
							safeToLocation(deCodeMain, pfadVonDatei);
						} catch (final FileNotFoundException ex) {
							ex.printStackTrace();
						} catch (final IOException ex) {
							ex.printStackTrace();
						}
					} else {
						safeToLocation(deCodeMain, pfadVonDatei);
					}

				} else {
					System.out.println("Ende... ");
				}

			} else {
				// der block dient dazu von einer bestehenden txt zu lesen und
				// dann zu deCode oder Code
				if (antwort.equals("N")) {
					System.out.println("Wollen Sie von einer Datei lesen?");
					antwort = leser.nextLine();
					if (antwort.equals("J")) {
						System.out.println("Gebe die Datei: ");
						zeile = leser.nextLine();

						Path pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
						if (Files.exists(pfadVonDatei)) {
							beforeDeCode = readFromLocation(pfadVonDatei);
							beforeCode = readFromLocation(pfadVonDatei);
						} else {
							leser.close();
							throw new FileNotFoundException("Datei existiert nicht");
						}

						System.out.println("Code or deCode?");
						antwort = leser.nextLine();
						if (antwort.equals("Code")) {
							System.out.println(kodieren(beforeCode, true));
						}

						if (antwort.equals("deCode")) {
							System.out.println(kodieren(beforeDeCode, false));
						}

					} else {
						System.out.println("Ende...");
					}
				}
			}
		}
		if (antwort.equals("N")) {
			System.out.println("Geben sie eine Datei ein:");
			zeile = leser.nextLine();
			Path pfadVonDatei = Paths.get(nameHomeVerzeichnis, zeile);
			if (Files.exists(pfadVonDatei)) {
				System.out.println("deCode oder Code?");
				antwort = leser.nextLine();
				if (antwort.equals("deCode")) {
					beforeDeCode = readFromLocation(pfadVonDatei);
					System.out.println(kodieren(beforeDeCode, false));
				} else {
					beforeCode = readFromLocation(pfadVonDatei);
					System.out.println(kodieren(beforeCode, true));
				}
			} else {
				System.out.println("Existiert nicht...Ende");
			}

		}
	}
}
