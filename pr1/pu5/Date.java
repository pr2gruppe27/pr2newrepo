package pr1.pu5;

import java.util.Calendar;
import java.util.Scanner;

/**
 * Aufgabe pr1pu5.2
 * @author YM-Performance
 *
 */

public class Date {

	private int day = 0;
	private int month = 0;
	private int year = 0;

	public Date(int d, int m, int y) {
		if (checkInput(d, m, y) == true) {
			this.day = d;
			this.month = m;
			this.year = y;
		}
	}

	// ***************************************+
	// Achtung getter methoden !!!
	public int getDay() {
		return this.day;
	}

	public int getMonth() {
		return this.month;
	}

	public int getYear() {
		return this.year;
	}
	// ***********************************

	public boolean equalsDate(Date date) {
		if (getYear() == date.getYear() && getMonth() == date.getMonth() && getDay() == date.getDay()) {
			return true;
		}

		else {
			return false;
		}
	}

	public boolean isBefore(Date date) {
		if (getYear() < date.year) {
			return false;
		}
		if (getYear() == date.year && getMonth() < date.month) {
			return false;
		}
		if (getYear() == date.year && getMonth() == date.month && getDay() < date.day) {
			return false;
		}

		return true;

	}

	public boolean isValid() {
		if (this.checkDate() > 7 || this.checkDate() < 1) {
			return false;
		}
		return true;
	}

	public boolean isAfter(Date date) {
		if (isBefore(date) == true) {
			return false;
		}
		return true;
	}

	public double checkDate() {
		int d = getDay();
		int m = getMonth();
		int y = getYear();
		if (m < 3)
			y = y - 1;
		var w = ((d + Math.floor(2.6 * ((m + 9) % 12 + 1) - 0.2) + y % 100 + Math.floor(y % 100 / 4)
				+ Math.floor(y / 400) - 2 * Math.floor(y / 100) - 1) % 7 + 7) % 7 + 1;
		return w;
	}

	private static boolean checkInput(int d, int m, int y) {
//System.out.println(d);
//System.out.println(m);
//System.out.println(y);
		Calendar calendar = Calendar.getInstance();
		// ***********
		// Achtung Month+1 weil er bei mir um eins zuruck geht
		// seltsam...
		int currentMonth = calendar.get(Calendar.MONTH) + 1;
		int currentYear = calendar.get(Calendar.YEAR);
		boolean leapYear = y % 4 == 0 && y != 1900;
		boolean MonthWith31Days = m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12;
		boolean MonthWith30Days = m == 4 || m == 6 || m == 9 || m == 11;
		boolean MonthWith29Days = m == 2 && leapYear;
		boolean MonthWith28Days = m == 2 && leapYear;
		if (y < 1900 || y > currentYear) {
//			System.out.println("1");
			return false;
		}
		if (m < 1 || m > 12) {
//			System.out.println("2");
			return false;
		}
		if (y == currentYear && m > currentMonth) {
//			System.out.println("3");
			return false;
		}
		if (d < 1) {
//			System.out.println("4");
			return false;
		}
		if (MonthWith31Days && d > 31) {
//			System.out.println("5");
			return false;
		}
		if (MonthWith30Days && d > 30) {
//			System.out.println("6");
			return false;
		}
		if (MonthWith29Days && d > 29) {
//			System.out.println("7");
			return false;
		}
		if (MonthWith28Days && d > 28) {
//			System.out.println("�8");
			return false;
		}
		return true;
	}

	public String toString() {
		return String.format("Tag: " + getDay() + " Monat: " + getMonth() + " Jahr: " + getYear());
	}

	public static void main(String[] args) {

		int d;
		int m;
		int y;
		Scanner leser = new Scanner(System.in);

		System.out.println("Gebe Tag: ");
		d = leser.nextInt();

		System.out.println("Gebe Monat: ");
		m = leser.nextInt();

		System.out.println("Gebe Jahr: ");
		y = leser.nextInt();

		leser.close();

		Date date = new Date(d, m, y);

		System.out.println(date);

	}

}
