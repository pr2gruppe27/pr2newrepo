package pr1.pu5;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author YM-Performance
 *
 */

public class PalindromTestCase {

	@Test
	public void testPalindromKonstruktor() {

		Palindrom eineZeichenkette = new Palindrom("Anna");
		Palindrom eineZeichenkette2 = new Palindrom("Lagerregal");
		Palindrom eineZeichenkette3 = new Palindrom("Otto");
		Palindrom eineZeichenkette4 = new Palindrom("Die Liebe ist Sieger; stets rege ist sie bei Leid.");
		Palindrom eineZeichenkette5 = new Palindrom("");
		
		assertEquals("", eineZeichenkette5.getWort());
		assertEquals("Anna", eineZeichenkette.getWort());
		assertEquals("Lagerregal", eineZeichenkette2.getWort());
		assertEquals("Otto", eineZeichenkette3.getWort());
		assertEquals("Die Liebe ist Sieger; stets rege ist sie bei Leid.", eineZeichenkette4.getWort());

	}

	@Test
	public void isPalindromTestTrue() {
		Palindrom eineZeichenkette = new Palindrom("Anna");
		Palindrom eineZeichenkette2 = new Palindrom("Lagerregal");
		Palindrom eineZeichenkette3 = new Palindrom("Otto");
		Palindrom eineZeichenkette4 = new Palindrom("Die Liebe ist Sieger; stets rege ist sie bei Leid.");
		Palindrom eineZeichenkette5 = new Palindrom("A");
		Palindrom eineZeichenkette6 = new Palindrom("a");
		Palindrom eineZeichenkette7 = new Palindrom("@@a");
		
		assertTrue(eineZeichenkette.isPalindrom());
		assertTrue(eineZeichenkette2.isPalindrom());
		assertTrue(eineZeichenkette3.isPalindrom());
		assertTrue(eineZeichenkette4.isPalindrom());
		assertTrue(eineZeichenkette5.isPalindrom());
		assertTrue(eineZeichenkette6.isPalindrom());
		assertTrue(eineZeichenkette7.isPalindrom());

	}

	@Test
	public void isPalindromTestFalse() {
		Palindrom eineZeichenkette = new Palindrom("");
		Palindrom eineZeichenkette2 = new Palindrom("@");
		Palindrom eineZeichenkette3 = new Palindrom("++##,.,.,   ");
		Palindrom eineZeichenkette4 = new Palindrom("Die  ist Sieger;  bei Leid.");

		assertFalse(eineZeichenkette.isPalindrom());
		assertFalse(eineZeichenkette2.isPalindrom());
		assertFalse(eineZeichenkette3.isPalindrom());
		assertFalse(eineZeichenkette4.isPalindrom());

	}

	@Test
	public void getWortTest() {
		Palindrom eineZeichenkette = new Palindrom("@AnnaB���P213123123");
		assertEquals("@AnnaB���P213123123", eineZeichenkette.getWort());

		eineZeichenkette.setWort("anna");

		assertEquals("@AnnaB���P213123123", eineZeichenkette.getWort());

	}

	@Test
	public void setWortTest() {
		Palindrom eineZeichenkette = new Palindrom("FU");
		eineZeichenkette.setWort("anna");
		assertTrue(eineZeichenkette.isPalindrom());

	}

}
