package pr1.pu5;
/**
 * 
 * @author YM-Performance
 *
 */

public class Palindrom {
	/**
	 * Panlindrom aufgabe 1 der Pflichtuebung. Ein Palindrom kann ein Satz- und
	 * Wort-Palindrom sein
	 * 
	 * * YM-Performance
	 */
	// Instanzvariablen
	protected String pali = "";
	protected String getWortPali = "";

	// Konstruktor Palindrom
	public Palindrom(String palindrom) {
		this.getWortPali = palindrom;
		this.pali = palindrom;
	}

	// liefert true wenn es ein Palindrom ist, sonst false
	public boolean isPalindrom() {
		String palin = "";
		String helpPalin = "";

		// dieser block dient dazu die zeichenkette palindrom
		// auf Aa und 0-9 zu reduzieren
		char[] paliChar = new char[this.pali.length()];
		for (int i = 0; i < this.pali.length(); i++) {
			// ASCII position 48 - 57 zahlen 0-9
			// TIP: ASCII tabelle anschauen zum besseren
			// verstandnis!
			if (this.pali.charAt(i) >= 48 && this.pali.charAt(i) <= 57) {
				paliChar[i] = this.pali.charAt(i);
			} else if (this.pali.charAt(i) >= 97 && this.pali.charAt(i) <= 122) {
				paliChar[i] = this.pali.charAt(i);

				// Grossbuchstaben umwandeln in kleinbuchstaben
				// in der ASCII tabelle heisst das einfach um den wert
				// 32 erhohen.
				// geht nur weil wir ein char[] haben und mit chars arbeiten
			} else if (this.pali.charAt(i) >= 65 && this.pali.charAt(i) <= 90) {
				paliChar[i] = this.pali.charAt(i);
				paliChar[i] = (char) (paliChar[i] + 32);
			}

		}
		// die Zeichenkette palin wird erstellt ohne lucken die durch
		// das aussortieren der nicht erlaubten zeichen passiert sind
		for (int i = 0; i <= paliChar.length - 1; i++) {
			if (paliChar[i] != 0) {
				palin += paliChar[i];
			}
		}
		// dieser block dient dazu die zeichenkette pali umzudrehen
		// und in eine zeichenkette helpPali zu speichern
		char[] paliChar2 = new char[palin.length()];
		for (int y = palin.length() - 1, i = 0; y >= 0; y--, i++) {
			paliChar2[i] = palin.charAt(y);
		}
		for (int i = 0; i < paliChar2.length; i++) {
			helpPalin += paliChar2[i];
		}

		// wir haben jetzt eine zeichenkette pali, welche nur gueltige
		// zeichen enthalt und eine zeichenkette helpPali die pali nur
		// umgekehrt ist
		// jetzt soll due Methode isPalindromRec aufgerufen werden die
		// final entscheidet ob es sich um ein Palindrom handelt.
		// die Methode wird recursiv ausgefuhrt
		boolean check = false;
		check = isPalindromRec(palin, helpPalin, 0);
		if (check == true) {
			return true;
		} else
			return false;

	}

	/*
	 * eine hilfsmethode um rekursiv zu schauen ob es sich um ein palindrom handet
	 * oder nicht wenn beide strings "" nichts drinnen steht dann ist es kein
	 * palindrom.
	 */
	private boolean isPalindromRec(String pali, String helpPali, int intervall) {
		if (pali != "" || helpPali != "") {
			// ******
			// die Strings sind nicht null, somit gehen wir in den vergleich
			if (pali.charAt(intervall) == helpPali.charAt(intervall)) {
				// wenn der vergleich positiv, dann soll eine position
				// weiter gegangen werden
				// dafur schauen wir erstmal, ob es noch eine gibt
				if (intervall + 1 < pali.length()) {
//				//rekursiver aufruf der methode , aber das intervall wird erhoht
					return isPalindromRec(pali, helpPali, intervall + 1);
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// liefert das Wort zuruck unabhangig davon ob es ein Palindrom ist
	// oder nicht
	public String getWort() {
		return this.getWortPali;
	}

	// setzt den String genauso wie der Konstruktor
	public void setWort(String palindrom) {
		this.pali = palindrom;

	}
}
