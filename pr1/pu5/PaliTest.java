package pr1.pu5;

import java.util.Scanner;
/**
 * 
 * @author YM-Performance
 *
 */

public class PaliTest {
	/**
	 * Diese Main dient zum Testen der Klasse Palindrom
	 * * YM-Performance
	 */

	public static void main(String[] args) {
		String pali = "";

		Scanner leser = new Scanner(System.in);
		System.out.println("Gebe Zeichenkette");
		pali = leser.nextLine();
		Palindrom palindrom = new Palindrom(pali);
		if (palindrom.isPalindrom() == true) {
			System.out.println("Zeichenkette ist ein Palindrom");
		} else {
			System.out.println("Zeichenkette ist kein Palindrom");
		}

		// setWort methoden Test
		System.out.println("Gebe Zeichenkette");
		pali = leser.nextLine();
		palindrom.setWort(pali);
		if (palindrom.isPalindrom() == true) {
			System.out.println("Zeichenkette ist ein Palindrom");
		} else {
			System.out.println("Zeichenkette ist kein Palindrom");
		}
		leser.close();
	}

}
