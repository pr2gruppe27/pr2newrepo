package pr1.pu3;

import java.util.Scanner;

public class Zahlensystem {
/**
 * in der aufgabe 1 blatt 3 soll eine beliebige zahl eingegebenwerden konnen
 * in der main und an die methode toBinary --> gibt die zahl
 * als binar code zuruck
 * @param zahl
 * @return
 */
	public static String toBinary(int zahl) {
		String binVerkehrtherum = "";
		int hilfsZahl = zahl;
		//in der schleife wird die zahl zum binar code
		//die zahl wird / 2 gerechnet solange is die zahl 0 ist
		while (zahl > 0) {
			//der / operator teilt aber ohne rest.
			zahl = zahl / 2;
			//die hilszahl wird geteilt aber der rest wird 
			//behalten
			binVerkehrtherum += "" + hilfsZahl % 2;
			//die hilfszahl wird wieder die eigentliche zahl
			//und die schleife wird fortgesetzt
			hilfsZahl = zahl;
		}
		
		//umdrehen des strings da verkehrtherum
		String binRichtig = "";
		for (int i = binVerkehrtherum.length(); i > 0; i--) {
			binRichtig += "" + binVerkehrtherum.charAt(i - 1);
		}

		return binRichtig;

	}

	public static void main(String[] args) {
		Scanner leser = new Scanner(System.in);
		int dieZahl=0;
		
		System.out.println("Geben sie eine dezi ein: ");
		dieZahl = leser.nextInt();
		System.out.println(toBinary(dieZahl));
		leser.close();

	}

}
