package pr1.pu3;

import java.util.Scanner;

public class Bruch {

	public static void bruchKuerzen(int zaehler, int nenner) {
		int zahl = 0;
		int zahl2 = 0;
		int a = zaehler;
		int b = nenner;

		if (a == 0 || b == 0) {
			System.out.println("Teilen mit 0 geht nicht");
		} else {

			do {
				zahl2 = zahl;
				zahl = groessterTeiler(a, b);
				if (a / zahl != 0 && b / zahl != 0) {
					a = a / zahl;
					b = b / zahl;
				} else {
					zahl = zahl2;
				}
			} while (zahl != zahl2);
			System.out.println(a);
			System.out.println("---");
			System.out.println(b);
		}
	}

	public static int groessterTeiler(int a, int b) {
		int[] arrayA = new int[a];
		int[] arrayB = new int[b];
		int zahl = 0;
		if (a == 0 || b == 0) {
			return 0;
		} else {
			if (a < 3) {
				arrayA[0] = 2;
			}
			if (b < 3) {
				arrayB[0] = 2;
			}

			for (int i = 1; i < a; i++) {
				if (a % i == 0) {
					arrayA[i] = i;
				}
			}
			for (int i = 1; i < b; i++) {
				if (b % i == 0) {
					arrayB[i] = i;
				}
			}
			for (int i = 0; i < arrayA.length; i++) {
				if (arrayA[i] != 0) {
					for (int i2 = 0; i2 < arrayB.length; i2++) {
						if (arrayB[i2] == arrayA[i]) {
							if (arrayB[i2] > zahl) {
								zahl = arrayB[i2];
							}
						}
					}
				}
			}

			return zahl;
		}
	}

	public static void main(String[] args) {
		Scanner leser = new Scanner(System.in);
		int zahl = 0;
		int zahl2 = 0;
		System.out.println("Geben sie einen Zaehler ein: ");
		zahl = leser.nextInt();
		System.out.println("Geben sie einen Nenner ein: ");
		zahl2 = leser.nextInt();
		bruchKuerzen(zahl, zahl2);
		leser.close();
	}

}
