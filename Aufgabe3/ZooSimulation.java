package io.dama.ffi.oo.polymorphie;

import io.dama.ffi.oo.polymorphie.tiere.Affe;
import io.dama.ffi.oo.polymorphie.tiere.Giraffe;
import io.dama.ffi.oo.polymorphie.tiere.Gorilla;
import io.dama.ffi.oo.polymorphie.tiere.ZooTier;

/**
 * Simulationsklasse.
 */
public final class ZooSimulation {

    /**
     * Konstruktor.
     */
    private ZooSimulation() {
        // keine Objekte benötigt
    }

    /**
     * Main-Methode.
     *
     * @param args Kommandozeilen-Argumente.
     */
    public static void main(final String[] args) {
        final var futterstelle = new Futterstelle();
        ZooTier[] tier = new ZooTier[3];

        tier[0] = new Affe("Charlie");
        tier[1] = new Gorilla("Buck");
        tier[2] = new Giraffe("Debbie");

        for (int i = 0; i < tier.length; i++) {
            System.out.println(tier[i]);
        }

        System.out.println("Fütterung...");
        for (int i = 0; i < tier.length; i++) {
            futterstelle.gibFutter(tier[i]);
        }

        for (int i = 0; i < tier.length; i++) {
            System.out.println(tier[i]);
        }
    }
}
