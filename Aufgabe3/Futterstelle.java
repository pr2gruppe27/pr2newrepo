package io.dama.ffi.oo.polymorphie;

import io.dama.ffi.oo.polymorphie.tiere.Affe;
import io.dama.ffi.oo.polymorphie.tiere.Giraffe;
import io.dama.ffi.oo.polymorphie.tiere.Gorilla;
import io.dama.ffi.oo.polymorphie.tiere.ZooTier;

/**
 * Fütterung der Tiere.
 */
public class Futterstelle {

    /**
     * 
     *
     * 
     */
    public void gibFutter(ZooTier tier) {
        tier.fuettern();
    }

}
