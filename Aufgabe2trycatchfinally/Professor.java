package io.dama.ffi.exceptions.trycatchfinally;

import java.awt.Point;

import de.smits_net.games.framework.board.Board;
import de.smits_net.games.framework.image.AnimatedImage;
import de.smits_net.games.framework.sprite.AnimatedSprite;
import de.smits_net.games.framework.sprite.Direction;

/**
 * Ein Sprite.
 */
public class Professor extends AnimatedSprite {

    /** Geschwindigkeit der Figur in X-Richtung. */
    private static final int SPRITE_SPEED = 2;

    /**
     * Neues Sprite anlegen.
     *
     * @param board      das Spielfeld
     * @param startPoint Start-Position
     */
    public Professor(final Board board, final Point startPoint) {
        super(board, startPoint, BoundaryPolicy.NONE,
                new AnimatedImage(50, 9, "assets/professor_left"));
        this.velocity.setVelocity(Direction.WEST, Professor.SPRITE_SPEED);
    }

    @Override
    public void move() {
        boolean wand = false;
        super.move();
try {
        if (this.position.x <= -20)
                 {
            wand = true;
            if(wand == true) {
                this.velocity.setVelocity(Direction.EAST, Professor.SPRITE_SPEED); 
                
                
            }
            throw new OuchException();
        }
        else if(this.position.x >= 360){
            wand = true;
            if(wand == true) {
                this.velocity.setVelocity(Direction.WEST, Professor.SPRITE_SPEED);
            }
        }
        
    }
    
    catch (Exception e) {
        System.out.printf("%s%n", e.getMessage());
        
    }
finally {
    if(wand==true)
    System.out.println(this.position.x);
}
    }
}
