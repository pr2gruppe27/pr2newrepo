package io.dama.ffi.oo.copy;

public class miMaMain {

	public static void main(String[] args) {
		Mitarbeiter dynamischManager = new Manager();
		Mitarbeiter dynamischManager2 = new Manager();
		Mitarbeiter derMitarbeiter = new Mitarbeiter();
		Manager derManager = new Manager();
		
		
		
		dynamischManager2 = derManager;
		dynamischManager = dynamischManager2;
		
		dynamischManager.gehtKaffeeTrinken(); //greift auf Statisches Objekt/Methode gehtKaffeeTrinken der Mitarbeiter Klasse
		dynamischManager2.gehtKaffeeTrinken();
		derMitarbeiter.gehtKaffeeTrinken(); // das selbe
		derManager.getMeeting(); // darf auf die methode zugreifen da es kein dynamisches objekt ist
		derManager.gehtKaffeeTrinken();
		
		if(dynamischManager2 instanceof Manager) {
			derManager = (Manager) dynamischManager2;
			derManager.getMeeting();
			}
		
		
		

	}

}
