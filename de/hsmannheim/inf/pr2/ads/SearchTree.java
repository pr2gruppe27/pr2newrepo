package de.hsmannheim.inf.pr2.ads;

import java.util.Iterator;

/**
 * Ein binärer Suchbaum.
 *
 * @author Markus Gumbel (m.gumbel@hs-mannheim.de)
 */
public class SearchTree<E extends Comparable<E>> implements Set<E> {

	protected SearchTreeNode<E> root; // Wurzelknoten dieses Suchbaums.

	/**
	 * Füge einen Wert in den Baum hinzu. Das geht nur, wenn dieser Wert noch nicht
	 * enthalten ist.
	 *
	 * @param o Einzufügendes Objekt.
	 * @return True, wenn Wert hinzugefügt wurde, oder false, wenn nicht, da dieser
	 *         schon vorhanden war.
	 */
	public boolean add(E o) {
		// Strategie: Wir suchen den Knoten, der nach dem Einfügen des neues
		// Elements der Elternknoten ist.

		TreeNode<E> parent = null; // (Vorläufiger) Elternknoten
		TreeNode<E> node = root; // (Vorläufiger) Kindknoten

		// Solange der aktuelle Kindknoten nicht null ist...
		while (node != null) {
			// Setze alten Kindknoten als neuen Elternknoten:
			parent = node;
			// Ist der Wert bereits in diesem Knoten gespeichert?
			if (node.getValue().equals(o)) {
				return false; // Ja, also kann er nicht nochmal eingefügt werden.
			} else if (o.compareTo(node.getValue()) < 0) {
				// Der einzufügende Wert ist kleiner als der aktuelle Knoten.
				node = node.getLeft(); // D.h. suche im linken Teilbaum weiter.
			} else {
				// Der einzufügende Wert ist größer als der aktuelle Knoten.
				node = node.getRight(); // D.h. suche im rechten Teilbaum weiter.
			}
		}
		// Erzeuge den neuen Knoten für den einzufügenden Wert:
		SearchTreeNode<E> newNode = new SearchTreeNode<>(o);
		if (parent == null) {
			// Kein Elternknoten gefunden. D.h. Baum ist leer.
			root = newNode; // Neuer Knoten wird Wurzelelement.
		} else if (o.compareTo(parent.getValue()) < 0) {
			// Hier wird festgestellt, ob der neue Knoten der rechte oder linke
			// Nachfolger des Elternknotens ist.
			parent.left = newNode; // In diesem Fall links.
		} else {
			parent.right = newNode; // Dito für rechts.
		}
		return true; // Neuer Wert konnte erfolgreich eingefügt werden.
	}

	/**
	 * Suche ein Element in dem Baum.
	 *
	 * @param o Objekt bzw. Suchschlüssel
	 * @return Wahr, wenn Wert im Baum vorhanden ist, falsch sonst.
	 */
	public boolean contains(E o) {
		// Iterative Variante
		TreeNode<E> n = root; // Erzeuge Zeiger, der bei root beginnt.

		while (n != null) {
			if (n.getValue().equals(o)) {
				return true; // Element gefunden.
			} else if (o.compareTo(n.getValue()) < 0) {
				// Suchschlüssel kleiner, also im linken Teilbaum weitersuchen.
				n = n.getLeft();
			} else {
				// Suchschlüssel größer, also im rechtenTeilbaum weitersuchen.
				n = n.getRight();
			}
		}
		return false; // Suche zu Ende, Wert nicht gefunden.
	}

	/**
	 * Entfernt einen Knoten mit dem Wert o aus dem Baum.
	 *
	 * @param o Objekt/Schlüssel, der entfernt werden soll
	 * @return True, falls Element entfernt wurde, false sonst.
	 */
	public boolean remove(E o) {
		// Noch nicht implementiert. Achtung: aufwändig!
		return false;
	}

	/**
	 * @return Anzahl der Elemente in dem Baum.
	 */
	public int size() {
		if (isEmpty()) { // Sonderfall leerer Baum beachten!
			return 0; // Leerer Baum hat Größe 0.
		} else {
			return root.size(); // Nimm Größe des fkt. Baums.
		}
	}

	/**
	 * Überprüft, ob der Baum Elemente enthält.
	 *
	 * @return Wahr, wenn der Baum leer ist, falsch sonst.
	 */
	public boolean isEmpty() {
		if (root == null) {
			return true;
		}
		return false;
	}

	/**
	 * Entfernt alle Elemente aus dem Baum.
	 */
	public void clear() {
		this.root = null;
	}

	/**
	 * Bestimme die Höhe des Baums.
	 *
	 * @return
	 */

	public void printInorder() {
		if (!isEmpty()) {
			root.printInorder();
		}
	}

	public Iterator<E> iterator() {
		return null;

	}

	public boolean containsRec(E o) {

		return containsRec2(root, o);
	}

	private boolean containsRec2(TreeNode<E> a, E o) {

		if (a.getValue().equals(o)) {
//			System.out.println("a: " + a);
//			System.out.println("b: " + o);
			return true;
		}

		else {

			if (a.getLeft() != null) {
				return containsRec2(a.getLeft(), o);
			}
			if (a.getRight() != null) {
				return containsRec2(a.getRight(), o);
			}

			return false;
		}

	}

	public List<E> sort() {
		List<E> list = new List<>();

		TreeNode<E> helpnode = root;
		fillList(helpnode, list);

		return list;
	}

	public void fillList(TreeNode<E> t, List<E> l) {
		if (t != null) {
			fillList(t.getLeft(), l);
			l.addLast(t.getValue());
			fillList(t.getRight(), l);
			
		}

	}

	public boolean isSearchTree() {
		if (root == null) {
			return false;
		}
		return isSearchTree(root);
	}

	public boolean isSearchTree(TreeNode<E> n) {
		if (n == null) {
			return true;
		}

		else {
			boolean b1, b2, b3, b4;
			b1 = true;
			if (n.getLeft() != null) {
				if (n.getLeft().getValue().compareTo(n.getValue()) > 0) {
					b1 = false;
				}
			}
			b2 = true;
			if (n.getRight() != null) {
				if (n.getRight().getValue().compareTo(n.getValue()) < 0) {
					b2 = false;
				}
			}
			b3 = isSearchTree(n.getLeft());
			b4 = isSearchTree(n.getRight());

			return b1 && b2 && b3 && b4;
		}
	}

	public boolean isAVLSearchTree() {
		return isAVLSearchTree(root);
	}

	private boolean isAVLSearchTree(TreeNode<E> n) {
//		System.out.println(n.balance());

		if (n.balance() < 0 || n.balance() > 1) {

			return false;
		}

		else {

			if (n.getLeft() != null) {
				return isAVLSearchTree(n.getLeft());
			}
			if (n.getRight() != null) {
				return isAVLSearchTree(n.getRight());
			}

			return true;
		}

	}

	public int height() {
		// Iterative Variante

		TreeNode<E> left = root.getLeft();
		TreeNode<E> right = root.getRight();
		if (right == null) {
			return height(left);
		}
		return height(right);

	}

	private int height(TreeNode<E> n) {
		if (n == null) {
			return 1;
		}

		int left = height(n.getLeft());
		int right = height(n.getRight());

		if (left > right) {
			return left + 1;
		} else {
			return right + 1;
		}

	}

}
