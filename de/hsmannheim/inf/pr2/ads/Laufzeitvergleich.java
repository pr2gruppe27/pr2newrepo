package de.hsmannheim.inf.pr2.ads;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.TreeSet;
/**
 * 
 * @author YM-Performance
 *
 */
public class Laufzeitvergleich {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Laufzeitvergleich test = new Laufzeitvergleich();

		int i = 100;
		System.out.println("100N test");
		test.zeitTreeSet(i);
		test.zeitHashSet(i);
		System.out.println("1000N test");
		i = 1000;
		test.zeitTreeSet(i);
		test.zeitHashSet(i);
		System.out.println("10000N test");
		i = 10000;
		test.zeitTreeSet(i);
		test.zeitHashSet(i);
		System.out.println("100000N test");
		i = 100000;
		test.zeitTreeSet(i);
		test.zeitHashSet(i);
		System.out.println("1000000N test");
		i = 1000000;
		test.zeitTreeSet(i);
		test.zeitHashSet(i);
		// **************
		// werte verdoppeln
		System.out.println("100*2N test");
		i = 100;
		test.zeitTreeSet(i * 2);
		test.zeitHashSet(i * 2);
		System.out.println("1000*2N test");
		i = 1000;
		test.zeitTreeSet(i * 2);
		test.zeitHashSet(i * 2);
		System.out.println("10000*2N test");
		i = 10000;
		test.zeitTreeSet(i * 2);
		test.zeitHashSet(i * 2);
		System.out.println("100000*2N test");
		i = 100000;
		test.zeitTreeSet(i * 2);
		test.zeitHashSet(i * 2);
		System.out.println("1000000*2N test");
		i = 1000000;
		test.zeitTreeSet(i * 2);
		test.zeitHashSet(i * 2);
		System.out.println("-----CONTAINS------");
		System.out.println("100N Test");
		i = 100;
//		String abc = "SKSDIEFJ";
		test.zeitTreeSetContains(i);
		test.zeitHashSetContains(i);
		System.out.println("1000N Test");
		i = 1000;
		test.zeitTreeSetContains(i);
		test.zeitHashSetContains(i);
		System.out.println("10000N Test");
		i = 10000;
		test.zeitTreeSetContains(i);
		test.zeitHashSetContains(i);
		System.out.println("100000N Test");
		i = 100000;
		test.zeitTreeSetContains(i);
		test.zeitHashSetContains(i);
		System.out.println("1000000N Test");

	}

	public boolean zeitTreeSetContains(int n) {
		TreeSet<String> ts1 = new TreeSet<String>();
		boolean ergebnis=false;
		
		//TreeSet erstellen mit der Anzahl N
		//die Strings sind alle mit fester lange 8
		//laut aufgabe soll der faktor N*2 sein
		//sodass die grosse des Sets doppent so gross ist
		
		var start = System.currentTimeMillis(); //zeitmessung wird gestartet
		for (int i = 0; i < n*2; i++) {
			ts1.add(RandomString.rString(8));
		}
        
        
        //nach dem erstellen soll N mal geschaut werden ob ein zufalliger string mit fester lange 8 in dem
        //set enthalten ist
		for(int i = 0 ; i< n ; i++) {
		 ergebnis = ts1.contains(RandomString.rString(8));
		}
		var ende = System.currentTimeMillis();
		var dauer = ende - start;
	
		System.out.println("TreeSet sagt: " + ergebnis+ " und hat gebraucht: "+dauer);
		return ergebnis;
	}

	public boolean zeitHashSetContains(int n) {
		HashSet<String> hs1 = new HashSet<>();
		boolean ergebnis=false;
		
		//HashSet erstellen mit der Anzahl N
				//die Strings sind alle mit fester lange 8
				//laut aufgabe soll der faktor N*2 sein
				//sodass die grosse des Sets doppent so gross ist
		
		var start = System.currentTimeMillis();
		for (int i = 0; i < n*2; i++) {

			hs1.add(RandomString.rString(8));
		}
		
		//nach dem erstellen soll N mal geschaut werden ob ein zufalliger string mit fester lange 8 in dem
        //set enthalten ist
        for(int i = 0; i<n;i++) {
		ergebnis = hs1.contains(RandomString.rString(8));
		}
        
		var ende = System.currentTimeMillis();
		var dauer = ende - start;

		System.out.println("HashSet sagt: " + ergebnis + " und hat gebraucht: " + dauer);
		return ergebnis;
	}

	public void zeitTreeSet(int n) {
//		System.out.println(n);
		TreeSet<String> ts1 = new TreeSet<String>();
		var start = System.currentTimeMillis();

		for (int i = 0; i < n; i++) {
			ts1.add(RandomString.rString(8));
		}

		var ende = System.currentTimeMillis();
		var dauer = ende - start;

		System.out.println("TreeSet braucht: " + dauer);

	}

	public void zeitHashSet(int n) {
		HashSet<String> hs1 = new HashSet<>();
		var start = System.currentTimeMillis();

		for (int i = 0; i < n; i++) {
			hs1.add(RandomString.rString(8));
		}

		var ende = System.currentTimeMillis();
		var dauer = ende - start;

		System.out.println("HashSet braucht: " + dauer);

	}

}
