package de.hsmannheim.inf.pr2.ads;

import java.util.Random;
/**
 * Zufall String Generator
 * 
 * @author YM-Performance
 *
 */
public class RandomString {


	
//die lange des String wird bestimmt mit n
	public static String rString(int n) {
		int counter = 0;

		//ABC...Z sind 26 zahlen. mit Random() wird eine zahl 
		//<26 gezogen und dann zu einem String zusammengebaut
		
		char[] charArray = new char[1];
		String[] stringArray = new String[1];
		String abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

		String neuString = "";
		while (counter < n) {

			charArray[0] = abc.charAt(new Random().nextInt(26));
			neuString += "" + charArray[0];
			counter++;
		}
		stringArray[0] = neuString;

		return stringArray[0];

	}

	public static void main(String[] args) {
//		String test = rString(15);
//		System.out.println(test);
	}

}
