package de.hsmannheim.inf.pr2.ads;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
/**
 * 
 * @author YM-Performance
 *
 * @param <E>
 */
public class HeapTreeNode<E extends Comparable<E>> extends TreeNode<E> implements Iterable<E> {

//	protected TreeNode<E> root;
	protected E value;
	protected TreeNode<E> left;
	protected TreeNode<E> right;

	public HeapTreeNode(E v, TreeNode<E> l, TreeNode<E> r) {
		super(v, l, r);
		this.value = v;
		this.left = l;
		this.right = r;

	}

	public ArrayList<E> heapArrayFill() {

		ArrayList<E> list = new ArrayList<>();

		list.add(0, this.getValue());
		fillList(this, list, 1);

		return list;
	}

	private void fillList(TreeNode<E> node, ArrayList<E> list, int i) {

		if (node.getRight() != null) {
			list.add(i, node.getRight().getValue());
			fillList(node.getRight(), list, 2 * i);
		}

		if (node.getLeft() != null) {
			list.add(i, node.getLeft().getValue());
			fillList(node.getLeft(), list, 2 * i + 1);
		}

	}

	public boolean isMaxHeapTree() {
		if (this.getValue() == null) {
			return false;
		}
		return isMaxHeapTree(this);
	}
/**
 * Die prufung ob es ein MaxHeapTree ist, muss jeden Knoten schauen ob
 * der Value im Kind Knoten kleiner ist als der im Eltern Knoten.
 * Wenn das fur den ganzen Baum gilt, wird true geliedert.
 * @param n
 * @return
 */
	private boolean isMaxHeapTree(TreeNode<E> n) {
		if (n == null) {

			return true;
		}

		else {
			boolean b1, b2, b3, b4;
			b1 = true;
			if (n.getLeft() != null) {
				if (n.getLeft().getValue().compareTo(n.getValue()) > 0) {
					b1 = false;
				}
			}
			b2 = true;
			if (n.getRight() != null) {
				if (n.getRight().getValue().compareTo(n.getValue()) > 0) {
					b2 = false;
				}
			}
			b3 = isMaxHeapTree(n.getLeft());
			b4 = isMaxHeapTree(n.getRight());

			return b1 && b2 && b3 && b4;
		}
	}
	/**
	 * Die prufung ob es ein MinHeapTree ist, muss jeden Knoten schauen ob
	 * der Value im Kind Knoten grosser ist als der im Eltern Knoten.
	 * Wenn das fur den ganzen Baum gilt, wird true geliedert.
	 * @param n
	 * @return
	 */
	public boolean isMinHeapTree() {
		if (this.getValue() == null) {
			return false;
		}

		return isMinHeapTree(this);
	}

	private boolean isMinHeapTree(TreeNode<E> n) {
		if (n == null) {

			return true;
		}

		else {
			boolean b1, b2, b3, b4;
			b1 = true;
			if (n.getLeft() != null) {
				if (n.getLeft().getValue().compareTo(n.getValue()) < 0) {
					b1 = false;
				}
			}
			b2 = true;
			if (n.getRight() != null) {
				if (n.getRight().getValue().compareTo(n.getValue()) < 0) {
					b2 = false;
				}
			}
			b3 = isMinHeapTree(n.getLeft());
			b4 = isMinHeapTree(n.getRight());

			return b1 && b2 && b3 && b4;
		}
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		ArrayList<E> list = new ArrayList<>(this.heapArrayFill());

		return new Iterator<E>() {
			int pos = 0;

			@Override
			public boolean hasNext() {
				return (pos < list.size());
			}

			@Override
			public E next() {
				pos = pos + 1;
				return list.get(pos - 1);

			}

		};
	}

}
