package de.hsmannheim.inf.pr2.ads;
/**
 * 
 * @author YM-Performance
 *
 * @param <E>
 */
public class Tripel<U extends Comparable<U>,V extends Comparable<V>,W extends Comparable<W>  >  {

	protected U u;
	protected V v;
	protected W w;

	public Tripel(U x, V y, W z) {
		this.u = x;
		this.v = y;
		this.w = z;
	}

	public U getU() {
		return this.u;
	}

	public V  getV() {
		return this.v;
	}

	public W getW() {
		return this.w;
	}

	public String toString() {
		return String.format("" + getU() + " " + getV() + " " + getW());
	}



}
