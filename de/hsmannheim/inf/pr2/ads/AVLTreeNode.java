package de.hsmannheim.inf.pr2.ads;
/**
 * 
 * @author YM-Performance
 *
 * @param <E>
 */
public class AVLTreeNode<E extends Comparable<E>> extends TreeNode<E> {

//	protected TreeNode<E> root;//das sollte weg
	// parameter stehn
	// konstruktor mit left und right futtern

	protected E value;
	protected TreeNode<E> left;
	protected TreeNode<E> right;

	public AVLTreeNode(E v, TreeNode<E> l, TreeNode<E> r) {
		super(v, l, r);
		this.value = v;
		this.left = l;
		this.right = r;
	}

	public boolean isSearchTree() {
		if (this.getValue() == null) {
			return false;
		}

		return isSearchTree(this);
	}
/**
 * Die prufung ob es ein SearchTree wird wie folgt gemacht
 * man geht durch die knoten des baums und schaut ob der linke kleiner 
 * als der eltern knoten ist und der rechte grosser als der eltern knoten
 * @param n
 * @return
 */
	private boolean isSearchTree(TreeNode<E> n) {

		if (n == null) {

			return true;
		}

		else {
			boolean b1, b2, b3, b4;
			b1 = true;
			if (n.getLeft() != null) {
				if (n.getLeft().getValue().compareTo(n.getValue()) > 0) {
					b1 = false;
				}
			}
			b2 = true;
			if (n.getRight() != null) {
				if (n.getRight().getValue().compareTo(n.getValue()) < 0) {
					b2 = false;
				}
			}
			b3 = isSearchTree(n.getLeft());
			b4 = isSearchTree(n.getRight());

			return b1 && b2 && b3 && b4;
		}
	}
/**
 * Ob es sich um einen AVLTree handelt wird wie folgt gepruft.
 * Man schaut ob jeder Teilknoten des Baums in Ballance ist.
 * Ein AVLTree ist ein AVLTree wenn die Ballance entweder 0 oder 1 ist. 
 * Die Ballance wird uber die methode ballance geholt.
 * @return
 */
	public boolean isAVLTree() {
		if (this.getValue() == null) {
			return false;
		}
		return isAVLSearchTree(this);
	}

	private boolean isAVLSearchTree(TreeNode<E> n) {
//		System.out.println(n.balance());

		if (n.balance() < 0 || n.balance() > 1) {

			return false;
		}

		else {

			if (n.getLeft() != null) {
				return isAVLSearchTree(n.getLeft());
			}
			if (n.getRight() != null) {
				return isAVLSearchTree(n.getRight());
			}

			return true;
		}

	}

}
