package de.hsmannheim.inf.pr2.ads.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.AVLTreeNode;
import de.hsmannheim.inf.pr2.ads.HeapTreeNode;
import de.hsmannheim.inf.pr2.ads.TreeNode;

class HeapTreeNodeTestCase {

	@Test
	void isHeapTreeNodeTest1(){
		HeapTreeNode heap = new HeapTreeNode(null,null,null);
		
		assertFalse(heap.isMinHeapTree());
		
	}
	@Test
	void isMinHeapTreeNodeTest2() {
		HeapTreeNode heap = new HeapTreeNode(600,new TreeNode(2,null,null),new TreeNode(3,null,null));
		
		assertFalse(heap.isMinHeapTree());
	}
	@Test
	void isMinHeapTreeNodeTest3() {
		HeapTreeNode heap = new HeapTreeNode(1,new TreeNode(2,null,null),new TreeNode(2,null,null));
		
		assertTrue(heap.isMinHeapTree());
	}
	@Test
	void isMinHeapTreeNodeTest4() {
		HeapTreeNode heap = new HeapTreeNode(1,new TreeNode(3,new TreeNode(4,new TreeNode(9,null,null),null),new TreeNode(4,null,null)),new TreeNode(2,new TreeNode(8,null,null),new TreeNode(7,null,null)));
		
		assertTrue(heap.isMinHeapTree());
	}
	
	@Test
	void isMaxHeapTreeNodeTest1() {
		HeapTreeNode heap = new HeapTreeNode(1,new TreeNode(3,new TreeNode(4,new TreeNode(9,null,null),null),new TreeNode(4,null,null)),new TreeNode(2,new TreeNode(8,null,null),new TreeNode(7,null,null)));
		
		assertFalse(heap.isMaxHeapTree());
	}
	@Test
	void isMaxHeapTreeNodeTest2() {
		HeapTreeNode heap = new HeapTreeNode(9,new TreeNode(7,new TreeNode(4,new TreeNode(1,null,null),null),new TreeNode(4,null,null)),new TreeNode(8,new TreeNode(2,null,null),new TreeNode(2,null,null)));
		
		assertTrue(heap.isMaxHeapTree());
	}
	@Test
	void isMaxHeapTreeNodeTest3() {
		HeapTreeNode heap = new HeapTreeNode(9,new TreeNode(8,null,null),new TreeNode(6,null,null));
		
		assertTrue(heap.isMaxHeapTree());
	}
	
	@Test
	void isMaxHeapTreeNodeTest4() {
		
		HeapTreeNode heap = new HeapTreeNode(null,null,null);
		assertFalse(heap.isMaxHeapTree());
	}
	@Test
	void heapArrayTest() {
//		TreeNode tree = new TreeNode(1,new TreeNode(2,null,null),new TreeNode(3,null,null));
		HeapTreeNode heap = new HeapTreeNode(2,new TreeNode(4,null,null),new TreeNode(6,null,null));
		
		ArrayList aL = new ArrayList();
		aL.add(2);
		aL.add(4);
		aL.add(6);
//		System.out.println(aL.toString());
//		System.out.println(heap.heapArrayFill().toString());
		assertEquals(aL.toString(),heap.heapArrayFill().toString());
	}
	@Test
	void heapArrayTest2() {
		HeapTreeNode heap = new HeapTreeNode(1,new TreeNode(3,new TreeNode(4,new TreeNode(9,null,null),null),new TreeNode(4,null,null)),new TreeNode(2,new TreeNode(8,null,null),new TreeNode(7,null,null)));
		
		ArrayList aL = new ArrayList();
		aL.add(1);
		aL.add(3);
		aL.add(2);
		aL.add(4);
		aL.add(4);
		aL.add(8);
		aL.add(7);
		aL.add(9);
//		System.out.println(aL.toString());
//		System.out.println(heap.heapArrayFill().toString());
		assertEquals(aL.toString(),heap.heapArrayFill().toString());
	}
	
	@Test
	void iteratorTest1() {
		HeapTreeNode heap = new HeapTreeNode(1,new TreeNode(3,new TreeNode(4,new TreeNode(9,null,null),null),new TreeNode(4,null,null)),new TreeNode(2,new TreeNode(8,null,null),new TreeNode(7,null,null)));
		//abc ist der name des iterators
		var abc = heap.iterator();
		int i = 0;
		while(i<heap.size()) {
		System.out.println(abc.next());
		i++;
		}
	}

}
