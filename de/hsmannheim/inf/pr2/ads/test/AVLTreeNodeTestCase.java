package de.hsmannheim.inf.pr2.ads.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.AVLTreeNode;
import de.hsmannheim.inf.pr2.ads.SearchTree;
import de.hsmannheim.inf.pr2.ads.TreeNode;

class AVLTreeNodeTestCase {

	@Test
	void isSearchTreeTest1() {
		AVLTreeNode avl = new AVLTreeNode(1, new TreeNode(200, null, null), new TreeNode(120, null, null));

//	System.out.println(tree.getValue());
		assertFalse(avl.isSearchTree());

	}

	@Test
	void isSearchTreeTest2() {
		AVLTreeNode avl = new AVLTreeNode(2, new TreeNode(1, null, null), new TreeNode(120, null, null));

//	System.out.println(tree.getValue());
		assertTrue(avl.isSearchTree());

	}

	@Test
	void isSearchTreeTest3() {
		AVLTreeNode avl = new AVLTreeNode(null, null, null);

		assertFalse(avl.isSearchTree());

	}

	@Test
	void isAVLSearchTreeTest1() {
		AVLTreeNode avl = new AVLTreeNode(null, null, null);

		assertFalse(avl.isAVLTree());

	}

	@Test
	void isAVLSearchTreeTest2() {

		AVLTreeNode avl = new AVLTreeNode(2, new TreeNode(1, null, null),
				new TreeNode(6, new TreeNode(4, new TreeNode(3, null, null), new TreeNode(5, null, null)),
						new TreeNode(7, null, null)));
		
		assertFalse(avl.isAVLTree());

	}

	@Test
	void isAVLSearchTreeTest3() {
		AVLTreeNode avl = new AVLTreeNode(1, null, new TreeNode(2, null, new TreeNode(3, null,
				new TreeNode(4, null, new TreeNode(5, null, new TreeNode(6, null, new TreeNode(7, null, null)))))));

		assertFalse(avl.isAVLTree());

	}

	@Test
	void isAVLSearchTreeTest4() {
		AVLTreeNode avl = new AVLTreeNode(4, new TreeNode(2, new TreeNode(1, null, null), new TreeNode(3, null, null)),
				new TreeNode(6, new TreeNode(5, null, null), new TreeNode(7, null, null)));

		assertTrue(avl.isAVLTree());

	}

}
