package de.hsmannheim.inf.pr2.ads.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.List;
import de.hsmannheim.inf.pr2.ads.SearchTree;
import de.hsmannheim.inf.pr2.ads.SearchTreeNode;
import de.hsmannheim.inf.pr2.ads.TreeNode;

class SearchTreeTestCase {

	@Test
	void isSearchTreeTest1() {
		var tree = new SearchTree();
		tree.add(2);
		tree.add(1);
		tree.add(6);
		tree.add(7);
		tree.add(4);
		tree.add(3);
		tree.add(5);
		assertTrue(tree.isSearchTree());

	}

	@Test
	void isSearchTreeTest2() {
		var tree = new SearchTree();

		tree.add(1);
		tree.add(2);
		tree.add(3);
		tree.add(4);
		tree.add(5);
		tree.add(6);
		tree.add(7);
		assertTrue(tree.isSearchTree());
	}

	@Test
	void isSearchTreeTest3() {
		var tree = new SearchTree();

		tree.add(4);
		tree.add(2);
		tree.add(6);
		tree.add(1);
		tree.add(3);
		tree.add(7);
		tree.add(5);
		assertTrue(tree.isSearchTree());
	}

	@Test
	void isSearchTreeTest4() {

		var tree = new SearchTree();
		assertFalse(tree.isSearchTree());

	}

	@Test
	void isAVLSearchTreeTest1() {
		var tree = new SearchTree();

		tree.add(2);
		tree.add(1);
		tree.add(6);
		tree.add(7);
		tree.add(4);
		tree.add(3);
		tree.add(5);

		assertEquals(4, tree.height());
		assertFalse(tree.isAVLSearchTree());

	}

	@Test
	void isAVLSearchTreeTest2() {
		var tree = new SearchTree();

		tree.add(1);
		tree.add(2);
		tree.add(3);
		tree.add(4);
		tree.add(5);
		tree.add(6);
		tree.add(7);

		assertEquals(7, tree.height());
		assertFalse(tree.isAVLSearchTree());

	}

	@Test
	void isAVLSearchTreeTest3() {
		var tree = new SearchTree();

		tree.add(4);
		tree.add(2);
		tree.add(6);
		tree.add(1);
		tree.add(3);
		tree.add(7);
		tree.add(5);

		assertEquals(3, tree.height());
		assertTrue(tree.isAVLSearchTree());

	}

	@Test
	void isEmptyTest() {
		// leerer baum
		var einSuchBaum = new SearchTree();
		assertTrue(einSuchBaum.isEmpty());
		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		assertFalse(einSuchBaum2.isEmpty());

	}

	@Test
	void clearTest() {

		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		einSuchBaum2.clear();
		assertTrue(einSuchBaum2.isEmpty());
		assertEquals(0, einSuchBaum2.size());

		var einSuchBaum3 = new SearchTree<Integer>();
		einSuchBaum3.add(1);
		einSuchBaum3.add(3);
		einSuchBaum3.add(4);
		einSuchBaum3.add(25);
		einSuchBaum3.clear();
		assertTrue(einSuchBaum3.isEmpty());
		assertEquals(0, einSuchBaum3.size());

	}
	
	@Test
	void heightTest2() {
		
		//die hohe nochmal kontrolliert mit selber erzeugten baumen
		//ohne methode add.
		//keine fehler feststellbar
	
		
		
		var einSuchBaum = new SearchTreeNode(2,new SearchTreeNode(1,null,null),new SearchTreeNode(6,new SearchTreeNode(4,new SearchTreeNode(3,null,null),new SearchTreeNode(5,null,null)),new SearchTreeNode(7,new SearchTreeNode(8,null,null),null)) );
		
		
		var einSuchBaum2 = new SearchTreeNode(1,null,new SearchTreeNode(3,null,new SearchTreeNode(4,null,new SearchTreeNode(25,null,null))));
		
		
		var einSuchBaum3 = new SearchTreeNode(2,new SearchTreeNode(1,null,null),new SearchTreeNode(3,new SearchTreeNode(25,null,null),null));
		
		assertEquals(4, einSuchBaum.height());
		assertEquals(4, einSuchBaum2.height());
		assertEquals(3,einSuchBaum3.height());
		
	}

	@Test
	void heightTest() {
		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		assertEquals(1, einSuchBaum2.height());

		var einSuchBaum3 = new SearchTree<Integer>();
		einSuchBaum3.add(1);
		einSuchBaum3.add(3);
		einSuchBaum3.add(4);
		einSuchBaum3.add(25);
		assertEquals(4, einSuchBaum3.height());

		var einSuchBaum4 = new SearchTree<Integer>();
		einSuchBaum4.add(10);
		einSuchBaum4.add(3);
		einSuchBaum4.add(25);
		einSuchBaum4.add(8);
		einSuchBaum4.add(24);
		einSuchBaum4.add(12);
		assertEquals(4, einSuchBaum4.height());

		var einSuchBaum5 = new SearchTree<Integer>();
		einSuchBaum5.add(1);
		einSuchBaum5.add(2);
		einSuchBaum5.add(3);
		einSuchBaum5.add(4);
		einSuchBaum5.add(5);
		einSuchBaum5.add(6);
		assertEquals(6, einSuchBaum5.height());

	}

	@Test
	void containsRecTest() {
		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		einSuchBaum2.add(3);
		einSuchBaum2.add(4);
		einSuchBaum2.add(58);
		assertTrue(einSuchBaum2.containsRec(3));
	}

	@Test
	void sortTest() {
		var einSuchBaum2 = new SearchTree<Integer>();
		var eineListe = new List<Integer>();
		eineListe.addLast(3);
		eineListe.addLast(4);
		eineListe.addLast(25);
		eineListe.addLast(58);
		einSuchBaum2.add(25);
		einSuchBaum2.add(3);
		einSuchBaum2.add(4);
		einSuchBaum2.add(58);
		
		assertEquals(eineListe.toString(), einSuchBaum2.sort().toString());
	}

}
