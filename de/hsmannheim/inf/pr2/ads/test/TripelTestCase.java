package de.hsmannheim.inf.pr2.ads.test;

import static org.junit.jupiter.api.Assertions.*;
/**
 * YM-Performance
 */

import java.util.ArrayList;
import java.util.Collections;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.Tripel;
import de.hsmannheim.inf.pr2.ads.TripelComparator;

class TripelTestCase {
/**
 * Test 1 wird geschaut ob Moglichkeit 1 also U->V->W aufsteigend 
 * richtig funktioniert
 */
	@Test
	void class1Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(200, "PP", 6.5));
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		
		Collections.sort(tripelList, new TripelComparator(1));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(2, "AA", 4.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(200, "PP", 6.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
	}
/**
 * 	Test 2 wird geschaut ob Moglichkeit 2 also U->V->W absteigend
 * 	richtig funktioniert
 */
		
	
	@Test
	void class2Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		tripelList.add(new Tripel(200, "PP", 6.5));
		Collections.sort(tripelList, new TripelComparator(2));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(200, "PP", 6.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(2, "AA", 4.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
		
	}
	
	/**
	 * 	Test 3 wird geschaut ob Moglichkeit 3 also V->U->W aufsteigend
	 * 	richtig funktioniert
	 */
	
	@Test
	void class3Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		tripelList.add(new Tripel(200, "PP", 6.5));
		Collections.sort(tripelList, new TripelComparator(3));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(2, "AA", 4.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(200, "PP", 6.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
	}
	
	/**
	 * 	Test 4 wird geschaut ob Moglichkeit 4 also V->U->W absteigend
	 * 	richtig funktioniert
	 */
	@Test
	void class4Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		tripelList.add(new Tripel(200, "PP", 6.5));
		Collections.sort(tripelList, new TripelComparator(4));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(200, "PP", 6.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(2, "AA", 4.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
		
	}
	
	/**
	 * 	Test 5 wird geschaut ob Moglichkeit 5 also W->V->U aufsteigend
	 * 	richtig funktioniert
	 */
	
	@Test
	void class5Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		tripelList.add(new Tripel(200, "PP", 6.5));
		Collections.sort(tripelList, new TripelComparator(5));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(2, "AA", 4.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(200, "PP", 6.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
		
	}
	
	/**
	 * 	Test 6 wird geschaut ob Moglichkeit 6 also W->V->U absteigend
	 * 	richtig funktioniert
	 */
	
	@Test
	void class6Test() {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(2, "AA", 5.5));
		tripelList.add(new Tripel(2, "AA", 4.5));
		tripelList.add(new Tripel(200, "PP", 6.5));
		Collections.sort(tripelList, new TripelComparator(6));
		
		ArrayList<Tripel> tripelList2 = new ArrayList<>();
		tripelList2.add(new Tripel(200, "PP", 6.5));
		tripelList2.add(new Tripel(2, "AA", 5.5));
		tripelList2.add(new Tripel(2, "AA", 4.5));
		
		assertEquals(tripelList2.toString(), tripelList.toString());
	}
	
	

}
