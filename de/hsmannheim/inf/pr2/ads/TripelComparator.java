package de.hsmannheim.inf.pr2.ads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
/**
 * 
 * @author YM-Performance
 *
 */

public class TripelComparator implements Comparator<Tripel> {


	final Integer n;

	public TripelComparator(int zahl) {
		this.n = zahl;

	}

	@Override
	public int compare(Tripel e1, Tripel e2) {
		// U aufsteigend
		if (n == 1) {
			// schauen ob U gleich
			if (e2.getU().compareTo(e1.getU()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getV().compareTo(e1.getV()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getW().compareTo(e1.getW()) == 0) {
						return 0;

						// <W> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getW().compareTo(e1.getW()) > 0) {
							return -1;
						}
						return 1;
					}
					// <V> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getV().compareTo(e1.getV()) > 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getU().compareTo(e1.getU()) > 0) {
				return -1;
			}
			return 1;
		}

		// U absteigend
		if (n == 2) {
			// schauen ob U gleich
			if (e2.getU().compareTo(e1.getU()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getV().compareTo(e1.getV()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getW().compareTo(e1.getW()) == 0) {
						return 0;

						// <W> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getW().compareTo(e1.getW()) < 0) {
							return -1;
						}
						return 1;
					}
					// <V> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getV().compareTo(e1.getV()) < 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getU().compareTo(e1.getU()) < 0) {
				return -1;
			}
			return 1;
		}

		// V aufsteigend
		if (n == 3) {
			// schauen ob V gleich
			if (e2.getV().compareTo(e1.getV()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getU().compareTo(e1.getU()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getW().compareTo(e1.getW()) == 0) {
						return 0;

						// <W> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getW().compareTo(e1.getW()) > 0) {
							return -1;
						}
						return 1;
					}
					// <U> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getU().compareTo(e1.getU()) > 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getV().compareTo(e1.getV()) > 0) {
				return -1;
			}
			return 1;
		}
		// V absteigend
		if (n == 4) {
			// schauen ob V gleich
			if (e2.getV().compareTo(e1.getV()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getU().compareTo(e1.getU()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getW().compareTo(e1.getW()) == 0) {
						return 0;

						// <W> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getW().compareTo(e1.getW()) < 0) {
							return -1;
						}
						return 1;
					}
					// <U> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getU().compareTo(e1.getU()) < 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getV().compareTo(e1.getV()) < 0) {
				return -1;
			}
			return 1;
		}
		// W aufsteigend
		if (n == 5) {
			// schauen ob W gleich
			if (e2.getW().compareTo(e1.getW()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getV().compareTo(e1.getV()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getU().compareTo(e1.getU()) == 0) {
						return 0;

						// <U> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getU().compareTo(e1.getU()) > 0) {
							return -1;
						}
						return 1;
					}
					// <V> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getV().compareTo(e1.getV()) > 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getW().compareTo(e1.getW()) > 0) {
				return -1;
			}
			return 1;
			// W absteigend
		} else {
			// schauen ob W gleich
			if (e2.getW().compareTo(e1.getW()) == 0) {
				// ja U gleich dann zum nachsten
				if (e2.getV().compareTo(e1.getV()) == 0) {
					// war auch gleich zum letzten, wenn gleich dann ret 0
					if (e2.getU().compareTo(e1.getU()) == 0) {
						return 0;

						// <U> wenn nicht gleich dann schauen ob grosser oder kleiner
					} else {
						if (e2.getU().compareTo(e1.getU()) < 0) {
							return -1;
						}
						return 1;
					}
					// <V> wenn nicht gleich dann schauen ob grosser oder kleiner
				} else {
					if (e2.getV().compareTo(e1.getV()) < 0) {
						return -1;
					}
					return 1;
				}

			}

			if (e2.getW().compareTo(e1.getW()) < 0) {
				return -1;
			}
			return 1;
		}
	}

	public static void main(String[] args) {
		ArrayList<Tripel> tripelList = new ArrayList<>();
		tripelList.add(new Tripel(4,"BB", 2.8));
		tripelList.add(new Tripel(200, "AA", 1.1));
		tripelList.add(new Tripel(1, "PP", 6.5));
		System.out.println("unsorted");
		tripelList.forEach(System.out::println);
		System.out.println("FIRST SORT");
		Collections.sort(tripelList, new TripelComparator(1));
		tripelList.forEach(System.out::println);
		System.out.println("SECOND SORT");
		Collections.sort(tripelList, new TripelComparator(2));
		tripelList.forEach(System.out::println);
		System.out.println("THIRD SORT");
		Collections.sort(tripelList, new TripelComparator(3));
		tripelList.forEach(System.out::println);
		System.out.println("4th SORT");
		Collections.sort(tripelList, new TripelComparator(4));
		tripelList.forEach(System.out::println);
		System.out.println("5th SORT");
		Collections.sort(tripelList, new TripelComparator(5));
		tripelList.forEach(System.out::println);
		System.out.println("6th SORT");
		Collections.sort(tripelList, new TripelComparator(6));
		tripelList.forEach(System.out::println);

	}

}
