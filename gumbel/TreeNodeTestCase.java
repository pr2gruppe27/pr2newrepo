package gumbel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.TreeNode;

class TreeNodeTestCase {

	@Test
	void equalStructureTest() {

		var neuerBaum = new TreeNode<>(1, new TreeNode<>(25), new TreeNode<>(50));
		var neuerBaum2 = new TreeNode<>(3, new TreeNode<>(4), new TreeNode<>(5));

//		System.out.println(neuerBaum.equalStructure(neuerBaum2));
		assertFalse(neuerBaum.equalStructure(neuerBaum2));
		System.out.println();
		var neuerBaum3 = new TreeNode<>(1, new TreeNode<>(25), new TreeNode<>(50));
		var neuerBaum4 = new TreeNode<>(1, new TreeNode<>(25), new TreeNode<>(50));

//		System.out.println(neuerBaum3.equalStructure(neuerBaum4));
		assertTrue(neuerBaum3.equalStructure(neuerBaum4));

		var neuerBaum5 = new TreeNode<>(1, new TreeNode<>(3, new TreeNode<>(7), new TreeNode<>(6)), new TreeNode<>(7));
		var neuerBaum6 = new TreeNode<>(1, new TreeNode<>(3, new TreeNode<>(7), new TreeNode<>(6)), new TreeNode<>(7));
		assertTrue(neuerBaum5.equalStructure(neuerBaum6));
		
		var neuerBaum7 = new TreeNode<>(1, new TreeNode<>(3, new TreeNode<>(7), new TreeNode<>(6)), new TreeNode<>(7));
		var neuerBaum8 = new TreeNode<>(1, new TreeNode<>(3, new TreeNode<>(7), new TreeNode<>(6)), new TreeNode<>(8));
		assertFalse(neuerBaum7.equalStructure(neuerBaum8));
		
		
	}

}
