package gumbel;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.List;
import de.hsmannheim.inf.pr2.ads.SearchTree;
import de.hsmannheim.inf.pr2.ads.SearchTreeNode;

class SearchTreeTestCase {

	@Test
	void isEmptyTest() {
		// leerer baum
		var einSuchBaum = new SearchTree();
		assertTrue(einSuchBaum.isEmpty());
		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		assertFalse(einSuchBaum2.isEmpty());

	}

	@Test
	void clearTest() {

		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		einSuchBaum2.clear();
		assertTrue(einSuchBaum2.isEmpty());
		assertEquals(0, einSuchBaum2.size());
		
		var einSuchBaum3 = new SearchTree<Integer>();
		einSuchBaum3.add(1);
		einSuchBaum3.add(3);
		einSuchBaum3.add(4);
		einSuchBaum3.add(25);
		einSuchBaum3.clear();
		assertTrue(einSuchBaum3.isEmpty());
		assertEquals(0, einSuchBaum3.size());
		
		
	}

	@Test
	void heightTest() {
		// baum mit einem wert
		var einSuchBaum2 = new SearchTree<Integer>();
		einSuchBaum2.add(25);
		assertEquals(0, einSuchBaum2.height());
		
		var einSuchBaum3 = new SearchTree<Integer>();
		einSuchBaum3.add(1);
		einSuchBaum3.add(3);
		einSuchBaum3.add(4);
		einSuchBaum3.add(25);
		assertEquals(3, einSuchBaum3.height());
		
		var einSuchBaum4 = new SearchTree<Integer>();
		einSuchBaum4.add(10);
		einSuchBaum4.add(3);
		einSuchBaum4.add(25);
		einSuchBaum4.add(8);
		einSuchBaum4.add(24);
		einSuchBaum4.add(12);
		assertEquals(3, einSuchBaum4.height());
		
		var einSuchBaum5 = new SearchTree<Integer>();
		einSuchBaum5.add(1);
		einSuchBaum5.add(2);
		einSuchBaum5.add(3);
		einSuchBaum5.add(4);
		einSuchBaum5.add(5);
		einSuchBaum5.add(6);
		assertEquals(5, einSuchBaum5.height());

	}
	
	@Test
	void containsRecTest() {
		// baum mit einem wert
				var einSuchBaum2 = new SearchTree<Integer>();
				einSuchBaum2.add(25);
				einSuchBaum2.add(3);
				einSuchBaum2.add(4);
				einSuchBaum2.add(58);
		assertTrue(einSuchBaum2.containsRec(3));		
	}

}
