package gumbel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import de.hsmannheim.inf.pr2.ads.List;
import de.hsmannheim.inf.pr2.ads.ListNode;

class ListTestCase {

	@Test
	public void toListStringTest() {
		List eineListe = new List();
		List eineListe2 = new List();

		ListNode<Integer> zuSuchen = new ListNode<>(1);
		ListNode<Integer> zuSuchen2 = new ListNode<>(1);
		ListNode<Integer> zuSuchen3 = new ListNode<>(2);
		ListNode<Integer> zuSuchen4 = new ListNode<>(3);
		ListNode<Integer> zuSuchen5 = new ListNode<>(5);
		ListNode<Integer> zuSuchen6 = new ListNode<>(8);

		ListNode zuSuchen7 = new ListNode<>("Hallo");
		ListNode zuSuchen8 = new ListNode<>("Welt");
		ListNode zuSuchen9 = new ListNode<>("das");
		ListNode zuSuchen10 = new ListNode<>("Wetter");
		ListNode zuSuchen11 = new ListNode<>("ist");
		ListNode zuSuchen12 = new ListNode<>("sch�n");

		eineListe.addLast(zuSuchen);
		eineListe.addLast(zuSuchen2);
		eineListe.addLast(zuSuchen3);
		eineListe.addLast(zuSuchen4);
		eineListe.addLast(zuSuchen5);
		eineListe.addLast(zuSuchen6);

		eineListe2.addLast(zuSuchen7);
		eineListe2.addLast(zuSuchen8);
		eineListe2.addLast(zuSuchen9);
		eineListe2.addLast(zuSuchen10);
		eineListe2.addLast(zuSuchen11);
		eineListe2.addLast(zuSuchen12);

		System.out.println(eineListe.getHead().toListString());
		System.out.println(eineListe2.getHead().toListString());
	}

	@Test
	public void containsIterTest() {

		List eineListe = new List();
		List eineListe2 = new List();

		ListNode<Integer> zuSuchen = new ListNode<>(1);
		ListNode<Integer> zuSuchen2 = new ListNode<>(1);
		ListNode<Integer> zuSuchen3 = new ListNode<>(2);
		ListNode<Integer> zuSuchen4 = new ListNode<>(3);
		ListNode<Integer> zuSuchen5 = new ListNode<>(5);
		ListNode<Integer> zuSuchen6 = new ListNode<>(8);

		ListNode zuSuchen7 = new ListNode<>("Hallo");
		ListNode zuSuchen8 = new ListNode<>("Welt");
		ListNode zuSuchen9 = new ListNode<>("das");
		ListNode zuSuchen10 = new ListNode<>("Wetter");
		ListNode zuSuchen11 = new ListNode<>("ist");
		ListNode zuSuchen12 = new ListNode<>("sch�n");

		eineListe.addLast(zuSuchen);
		eineListe.addLast(zuSuchen2);
		eineListe.addLast(zuSuchen3);
		eineListe.addLast(zuSuchen4);
		eineListe.addLast(zuSuchen5);
		eineListe.addLast(zuSuchen6);

		eineListe2.addLast(zuSuchen7);
		eineListe2.addLast(zuSuchen8);
		eineListe2.addLast(zuSuchen9);
		eineListe2.addLast(zuSuchen10);
		eineListe2.addLast(zuSuchen11);
		eineListe2.addLast(zuSuchen12);

		assertTrue(zuSuchen.containsIter(zuSuchen.value));
		assertTrue(zuSuchen2.containsIter(zuSuchen2.value));
		assertTrue(zuSuchen3.containsIter(zuSuchen3.value));
		assertTrue(zuSuchen12.containsIter(zuSuchen12.value));

	}

	@Test
	public void containsTest() {
		// Liste1 ***********************
		List eineListe = new List();
		List eineListe2 = new List();

		ListNode<Integer> zuSuchen = new ListNode<>(1);
		ListNode<Integer> zuSuchen2 = new ListNode<>(1);
		ListNode<Integer> zuSuchen3 = new ListNode<>(2);
		ListNode<Integer> zuSuchen4 = new ListNode<>(3);
		ListNode<Integer> zuSuchen5 = new ListNode<>(5);
		ListNode<Integer> zuSuchen6 = new ListNode<>(8);

		ListNode zuSuchen7 = new ListNode<>("Hallo");
		ListNode zuSuchen8 = new ListNode<>("Welt");
		ListNode zuSuchen9 = new ListNode<>("das");
		ListNode zuSuchen10 = new ListNode<>("Wetter");
		ListNode zuSuchen11 = new ListNode<>("ist");
		ListNode zuSuchen12 = new ListNode<>("sch�n");

		eineListe.addLast(zuSuchen);
		eineListe.addLast(zuSuchen2);
		eineListe.addLast(zuSuchen3);
		eineListe.addLast(zuSuchen4);
		eineListe.addLast(zuSuchen5);
		eineListe.addLast(zuSuchen6);

		eineListe2.addLast(zuSuchen7);
		eineListe2.addLast(zuSuchen8);
		eineListe2.addLast(zuSuchen9);
		eineListe2.addLast(zuSuchen10);
		eineListe2.addLast(zuSuchen11);
		eineListe2.addLast(zuSuchen12);

		assertTrue(zuSuchen.contains(zuSuchen.value));
		assertTrue(zuSuchen2.contains(zuSuchen2.value));
		assertTrue(zuSuchen3.contains(zuSuchen3.value));
		assertTrue(zuSuchen4.contains(zuSuchen4.value));
		assertTrue(zuSuchen12.contains(zuSuchen12.value));

	}

}
