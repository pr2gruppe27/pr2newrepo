package gumbel;

import java.util.Iterator;

/**
 * Eine einfach verkettete Liste.
 *
 * @author Markus Gumbel (m.gumbel@hs-mannheim.de)
 */
public class List<E> implements Container<E> {
	/**
	 * Listenkopf.
	 */
	protected ListNode<E> head = null;

	/**
	 * Falls bereits ein Element in neuer Liste sein soll.
	 *
	 * @param value Wert des ersten Elements.
	 */
	public List(E value) {
		head = new ListNode(value, null);
	}

	/**
	 * Erzeugt eine leere Liste.
	 */
	public List() {
	}

	public void addFirst(E e) {
		if (isEmpty()) {
			head = new ListNode(e); // neuer Head-Knoten.
		} else {
			head = head.addFirst(e);
		}
	}

	/**
	 * Fügt ein Element an das Ende der Liste an.
	 *
	 * @param e Element, das eingefügt werden soll.
	 */
	public void addLast(E e) {
		ListNode<E> node = new ListNode<>(e); // Erzeuge Knoten.
		// Das Ende der Liste suchen:
		ListNode<E> p = head; // Hilfsvariable.
		if (p == null) { // Leere Liste?
			head = node; // Head ist jetzt der neue Knoten.
		} else { // Liste enthält Elemente.
			while (p.getTail() != null) { // p am Ende?
				p = p.getTail(); // p wandert weiter.
			}
			p.next = node; // Füge am Ende an.
		}
	}

	/**
	 * Überprüft, ob die Liste Elemente enthält.
	 *
	 * @return Wahr, wenn die Liste leer ist, falsch sonst.
	 */
	public boolean isEmpty() {
		// Es könnte auch überprüft werden, ob size() 0 liefert,
		// dieser Test ist jedoch schneller:
		return head == null;
	}

	/**
	 * Entfernt alle Elemente aus der Liste.
	 */
	public void clear() {
		head = null;
	}

	/**
	 * @return Anzahl der Elemente in der Liste.
	 */
	public int size() {
		if (head != null) {
			return head.size();
		} else {
			return 0;
		}
	}

	/**
	 * Erzeugt eine (kurze) Textdarstellung der Liste.
	 *
	 * @return Textdarstellung der Liste.
	 */
	public String toString() {
		// Anmerkung: StringBuffer wäre die bessere Lösung.
		String text = "";
		ListNode<E> p = head;
		while (p != null) {
			text += p.toString() + " ";
			p = p.getTail();
		}
		return "( " + text + ") ";
	}

	// --- Einige Methoden sind noch nicht implementiert. ---

	/**
	 * @return Der Wert des ersten Elements oder eine ContainerException, wenn die
	 *         Liste leer ist.
	 */
	public E getFirst() {
		// Bitte fertig ausprogrammieren:
		if (this.head != null) {
			return head.getHead();
		} else {
			throw new ContainerException("Der Kopf ist leer");
		}
	}

	/**
	 * @return Der Wert des letzten Elements oder eine ContainerException, wenn die
	 *         Liste leer ist.
	 */
	public E getLast() {
		// Bitte fertig ausprogrammieren:
		ListNode<E> p = head; // Hilfsvariable.
		if (p == null) { // Leere Liste?
			throw new ContainerException("Die Liste ist leer");
		} else { // Liste enth�lt Elemente.
			while (p.getTail() != null) { // p am Ende?
				p = p.getTail(); // p wandert weiter.
			}

		}
		return p.value;
	}

	/**
	 * Entfernt das erste Element aus der Liste.
	 *
	 * @return Der Wert des ersten Elements oder eine ContainerException, wenn die
	 *         Liste leer ist.
	 */
	public E removeFirst() {
		// Bitte fertig ausprogrammieren:
		ListNode helpNode;
		helpNode = head;
		if (this.head != null && this.head.next != null) {
			this.head = this.head.next;
			return head.getHead();
		} else {
			throw new ContainerException("Der Kopf ist leer");
		}
	}

	/**
	 * Entfernt das letzte Element aus der Liste.
	 *
	 * @return Der Wert des letzten Elements oder eine ContainerException, wenn die
	 *         Liste leer ist.
	 */
	public E removeLast() {
		// Bitte fertig ausprogrammieren:
		ListNode<E> p = head; // Hilfsvariable.
		if (p == null) { // Leere Liste?
			throw new ContainerException("Die Liste ist leer");
		} else { // Liste enth�lt Elemente.
			while (p.getTail().getTail() != null) { // p am Ende?
				p = p.getTail(); // p wandert weiter.
			}
			p.next = null;
		}
		return p.value;
	}

	/**
	 * @param idx
	 * @return Wert an Position <code>idx</code> oder IndexOutOfBoundsException,
	 *         wenn der Index ungültig ist.
	 */
	public E getAt(int idx) {
		// Bitte fertig ausprogrammieren:
		ListNode<E> p = head; // Hilfsvariable.
		if (idx > this.size()) {
			throw new IndexOutOfBoundsException("Die Liste ist kleiner als der Wert");
		} else if (idx == this.size()) {

			return this.getLast();
		} else if (idx == 0) {
			return this.getFirst();
		} else {
			int counter = idx;
			while (p.getTail() != null && counter > 0) { // p am Ende?
				p = p.getTail(); // p wandert weiter.
				counter--;
			}
			return p.value;
		}
	}

	/**
	 * Überschreibe den Wert an Position <code>idx</code> mit <code>value</code>.
	 * Es wird eine Fehlermeldung ausgegeben, wenn <code>idx</code> eine üngültige
	 * Position ist.
	 *
	 * @param idx
	 * @param value
	 */
	public void setAt(int idx, E value) {
		// Bitte fertig ausprogrammieren:
		ListNode<E> p = head; // Hilfsvariable.

		if (idx > this.size()) {
			throw new IndexOutOfBoundsException("Die Liste ist kleiner als der Wert");
		} else if (idx == this.size()) {
			p = (ListNode<E>) this.getLast();
			p.value = value;
		} else if (idx == 0) {
			throw new IndexOutOfBoundsException(">0!");
		} else if (idx == 1) {
			this.head.value = value;
		} else {
			int counter = idx - 1;

			while (p.getTail() != null && counter > 0) { // p am Ende?
				p = p.getTail(); // p wandert weiter.
				counter--;
			}
			p.value = value;
		}
	}

	/**
	 * Löscht alle Elemente mit diesem Wert aus der Liste.
	 * 
	 * @param e Zu löschender Wert.
	 * @return Wahr, falls mindestens ein Element gelöscht wurde oder falsch, wenn
	 *         nicht.
	 */
	public boolean remove(E e) {
		// Bitte fertig ausprogrammieren:
		return false;
	}

	/**
	 * Löscht ein Element an der Position idx.
	 * 
	 * @param idx
	 * @return
	 */
	public E removeAt(int idx) {
		// Bitte fertig ausprogrammieren:
		return null;
	}

	public ListNode<E> getHead() {
		return this.head;
	}

	/**
	 * Überprüft, ob der der Wert <code>e</code> in der Liste enthalten ist.
	 *
	 * @param e
	 * @return
	 */
	public boolean contains(E e) {
		// Bitte fertig ausprogrammieren:
		return false;
	}

	/**
	 * Fügt eine andere Liste an das Ende dieser Liste an. Achtung! Die angefügte
	 * Liste könnte auch weiterhin doppelt verwendet werden. Das wird hier NICHT
	 * gemacht, statt dessen wird die ursprüngliche Liste geleert.
	 *
	 * @param otherList Die andere Liste.
	 */
	public void concat(List<E> otherList) {
		// Bitte fertig ausprogrammieren:
	}

	public Iterator<E> iterator() {
		// Bitte fertig ausprogrammieren:
		ListNode currentNode = head;

		return new Iterator<E>() {
			int pos = 0;
			ListNode<E> p = head;

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				if ((pos < currentNode.size()))
					return true;
				return false;
			}

			@Override
			public E next() {

				if (pos == 0) {
					p = head;
				} else {
					p = p.next;
				}
				pos++;

				return p.value;
			}

		};

	}

}
