package Hallo_Mannheim;

public class Array {

	public static void main(String[] args) {

//		int[] intArray = new int[3];
//
//		intArray[0] = 0+1;
//		intArray[1] = 1+1;
//		intArray[2] = 2+1;
//		System.out.println("vor array +3: " + intArray[1]);
//		intArray[1] = intArray[1] + 3;
//		System.out.println("nach +3: " + intArray[1]);
//		intArray[1] = 0;
//		System.out.println("nach  = 0: " + intArray[1]);
//
//		System.out.println(intArray[1]);

		// *******************************************************

		int[] intArray2 = new int[3];
		for (int i = 0; i < intArray2.length; i++) {
			intArray2[i] = i + 1;
		}

//		for(int i = 0; i< 3;i++) {
//			System.out.println(intArray2[i]);
//		}

		// zahle das array hoch bis zur stelle 2
		// stelle 2 = array[0] ist ja die 1. stelle
		// stelle 2 = im array[1]

//		for (int i = 0; i <= 2; i++) {
//			if (i == 2) {
//				intArray2[i - 1] = 5;
//			}
//		}
//		int counter = 0;
//		while (counter <= 2) {
//			if (counter == 2) {
//				intArray2[counter] = 5;
//			}
//			counter++;
//		}
//
//		for (int i = 0; i < 3; i++) {
//			System.out.println(intArray2[i]);
//		}

		// *********************************************
		// objekte
		String[] stringArray = new String[3];
		stringArray[0] = "Bus1";
		stringArray[1] = "Bus2";
		stringArray[2] = "Bus3";
		String bus = "Bus3";

		// objekt loschen im array
//		stringArray[1] = null;

//		for (int i = 0; i < 3; i++) {
//			if (stringArray[i] != null) {
//				System.out.println(stringArray[i]);
//			}
//		}

//		boolean gefunden = false;
//		for (int i = 0; i < 3; i++) {
//			if (stringArray[i] != null) {
//				if (stringArray[i].equals(bus)) {
//					gefunden = true;
//					stringArray[i] = null;
//				} else {
//					gefunden = false;
//				}
//			}
//		}
//
//		for (int i = 0; i < 3; i++) {
//
//			System.out.println(stringArray[i]);
//		}
//
//		System.out.println(gefunden);
//

		// ***************************************************
//		                           012
		String eineZeichenkette = "ABC";
		String neu = "";
		char[] stringArray3 = new char[eineZeichenkette.length()];

		for (int i = eineZeichenkette.length() - 1, y = 0; i >= 0; y++, i--) {
			stringArray3[y] = eineZeichenkette.charAt(i);
		}

		// array in einen String befullt
		for (int i = 0; i < 3; i++) {
			neu += "" + stringArray3[i];
		}

		System.out.println(neu);

		

//		neu = "" + eineZeichenkette.charAt(2);
////		neu = neu+ "" + eineZeichenkette.charAt(eineZeichenkette.length()-1);
//		neu += "" + eineZeichenkette.charAt(2-1);
////		neu = neu+"" + eineZeichenkette.charAt(eineZeichenkette.length()-2);
//		neu += "" + eineZeichenkette.charAt(2-2);
////		neu = neu+"" + eineZeichenkette.charAt(eineZeichenkette.length()-3);
//		System.out.println(neu);

//		for(int i = eineZeichenkette.length()-1 ; i>=0; i--) {
//			neu = neu + eineZeichenkette.charAt(i);
//		}
//		
//		System.out.println(neu);

	}

}
