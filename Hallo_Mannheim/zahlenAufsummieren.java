package Hallo_Mannheim;

import java.util.*;

public class zahlenAufsummieren {

	public static void main(String[] args) {
		Scanner leser = new Scanner(System.in);
		int zahlenSumme;
		int ausgabe = 0;
		boolean fertig = false;

		while (fertig == false) {
			System.out.println("Gebe eine Zahl: ");
			zahlenSumme = leser.nextInt();
			if (zahlenSumme % 2 == 0) {
				ausgabe = ausgabe + zahlenSumme;
			}
			if (zahlenSumme < 0) {
				fertig = true;
			}
		}
		System.out.println(ausgabe);
		leser.close();

	}

}
