package pr1aufgaben;

import java.util.*;

public class verschluesselung {

	public static void main(String[] args) {
		Scanner leser = new Scanner(System.in);
		String eingabe;

		System.out.println("Gebe eine Zeichenkette: ");
		eingabe = leser.nextLine();
		leser.close();
		System.out.println(deCode(eingabe));

	}

	public static String deCode(String eingabe) {
		String eingabeNeu = "";
		char[] portion = new char[eingabe.length()];
		for (int i = 0; i < eingabe.length(); i++) {

			portion[i] = eingabe.charAt(i);
		}
		int counter = 0;
		char help;
		while (counter < portion.length) {
			help = portion[counter];
			if (help == 'Z' || help == 'z') {
				help = (char) (help - 25);
				portion[counter] = help;
			} else if (istBuchstabe(help) == true) {
				help = (char) (help + 1);
				portion[counter] = help;
			}
			counter++;
		}

		for (int i = 0; i < portion.length; i++) {
			eingabeNeu += "" + portion[i];
		}

		return eingabeNeu;
	}

	public static boolean istBuchstabe(char buchstabe) {
		boolean ja = false;
		if (buchstabe == 'A' || buchstabe == 'a' || buchstabe == 'B' || buchstabe == 'b' || buchstabe == 'C'
				|| buchstabe == 'c' || buchstabe == 'D' || buchstabe == 'd' || buchstabe == 'E' || buchstabe == 'e'
				|| buchstabe == 'F' || buchstabe == 'f' || buchstabe == 'G' || buchstabe == 'g' || buchstabe == 'H'
				|| buchstabe == 'h' || buchstabe == 'I' || buchstabe == 'i' || buchstabe == 'J' || buchstabe == 'j'
				|| buchstabe == 'K' || buchstabe == 'k' || buchstabe == 'L' || buchstabe == 'l' || buchstabe == 'M'
				|| buchstabe == 'm' || buchstabe == 'N' || buchstabe == 'n' || buchstabe == 'O' || buchstabe == 'o'
				|| buchstabe == 'P' || buchstabe == 'p' || buchstabe == 'Q' || buchstabe == 'q' || buchstabe == 'R'
				|| buchstabe == 'r' || buchstabe == 'S' || buchstabe == 's' || buchstabe == 'T' || buchstabe == 't'
				|| buchstabe == 'U' || buchstabe == 'u' || buchstabe == 'V' || buchstabe == 'v' || buchstabe == 'W'
				|| buchstabe == 'w' || buchstabe == 'X' || buchstabe == 'x' || buchstabe == 'Y' || buchstabe == 'y'
				|| buchstabe == 'Z' || buchstabe == 'z')
			ja = true;
		return ja;

	}

}
